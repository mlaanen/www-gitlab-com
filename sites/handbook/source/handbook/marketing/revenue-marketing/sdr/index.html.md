---
layout: handbook-page-toc
title: "Sales Development"
description: "As a Sales Development Representative (SDR), you focus on outreach, prospecting, and lead qualification."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

As a Business Development Associate (BDA), Sales Development Representative (SDR), or Business Development Representative (BDR) in the Sales Development Organization you need to have an understanding of not only product and industry knowledge, but also sales soft skills, and internal [tools](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/sales-development-tools/) and processes. This handbook page will act as a guide to those topics as well as general information about the Sales Development Organization.

## Reaching the Sales Development Organization (internally)

#### Slack Channels (by region)

* **Main Channel- (Global Manager - Jean-Baptiste "JB" Larramendy)** = [`#sdr_global`](https://gitlab.slack.com/messages/C2V1KLY0Z)      
* **Announcements - (SDR Enablement - Chris Wang)** = [`#sdr-fyi`](https://app.slack.com/client/T02592416/C011P828JRL)            

**AMER**
* **All** = [`#sdr_amer`](https://gitlab.slack.com/messages/CM2GAVC78)      
* **SDR AMER (Manager- Allison Graban)** = [`#sdr_amer_inbound`](https://gitlab.slack.com/archives/C02AVVBFUAW)
* **BDR AMER Mid-Market (Manager - Sunanda Nair)** = [`#sdr_amer_mm`](https://app.slack.com/client/T02592416/C014PHFNE2U)
* **BDR Enterprise Expand AMER (Manager - Mauricio Nogales)** = [`#sdr_amer_ent_expand`](https://gitlab.slack.com/archives/CUFRP6U6Q)
* **BDR Enterprise Land West (Interim and Lead Land Manager - Meaghan Thatcher)** = [`#sdr_amer_ent_land_east`](https://gitlab.slack.com/archives/C01LK4EFDNW)
* **BDR PubSec (Manager - Josh Downey)** = [`#sdr_amer_pubsec`](https://gitlab.slack.com/archives/C02EBM1FN00)
* **Enterprise Land East (Manager - Shamit Paul)** = [`#sdr_amer_ent_land_east`](https://gitlab.slack.com/archives/C01LK4EFDNW)

**EMEA**
* **All** = [`#sdr_emea`](https://gitlab.slack.com/messages/CCULKLB71)   
* **SDR EMEA - (Manager - Glenn Perez)**=    
* **BDR Mid-Market - (Manager - Panos Rodopoulos)** = [`#sdr-emea-mm`](https://gitlab.slack.com/messages/CM0BYV7CM)
* **BDR Enterprise Expand - (Manager - Christina Souleles)** = [`#sdr_emea_expand`](https://gitlab.slack.com/archives/C01LHENNLKD)
* **BDR Enterprise Land - (Manager - Miguel Nunes)** = [`#sdr_emea_land`](https://gitlab.slack.com/archives/C01M76N9NTA)

**APAC**
* **SDR APAC - (Manager - Glenn Perez)** = [`#sdr_apac`](https://gitlab.slack.com/messages/CM0BPBEQM)
* **BDR APAC - (Manager - Jessica Wong)** = [`#bdr_apac`](https://gitlab.slack.com/archives/C0327R1NZBK)

**Please acknowledge any Slack messages from your managers in your Slack channels, if you have nothing to say at least be creative and funny with emojis so they know you are seeing their requests and updates and are not talking to an empty room!**

## Quick Reference Guide

| Resource |
| :----: |
|  [Sales Development Org Onboarding page](/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/) |
|  [Tanuki Tech](/handbook/marketing/revenue-marketing/sdr/tanuki-tech/) |
|  [Sales Development Org Edcast channel](https://gitlab.edcast.com/channel/sdr) |
|  [Sales handbook page](/handbook/sales/) |
|  [Go to Market page](/handbook/sales/field-operations/gtm-resources/) |
|  [Sales Development Org Manager resources page](/handbook/marketing/revenue-marketing/sdr/sdr-manager-resources) |
|  [Sales Development Org job family/levels](https://about.gitlab.com/job-families/marketing/sales-development-representative/) |


## Sales Development Org Training & Development

#### Onboarding
In your first month at GitLab we want to help ensure you have everything you need to be successful in your job. You will go through enablement videos, live sessions, and activities covering a wide range of getting started topics.
- [BDR/SDR onboarding goals and process](/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/)

#### Sales Development Org Enablement

The Sales Development Organization will conduct enablement and training on a wide range of topics such as workflow/processes, campaigns, alignment with other teams, tool training, product training, etc. BDR/SDR enablement sessions are scheduled on an as needed basis and will be made available for BDR/SDR teams to consume asynchronously.

To view previously recorded BDR/SDR Enablement content, you can view the [BDR/SDR Enablement Video Library here](https://www.youtube.com/playlist?list=PL05JrBw4t0KrjbznnEEiCtxUfT8-OV6X8)

- If you would like to request or run an enablement session on a specific topic, please fill out [this issue](https://gitlab.com/gitlab-com/marketing/sdr/-/issues/new?issuable_template=sdr_enablement_series_request).

#### Sales Development Technical Development
As part of your [onboarding](/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/), you will begin an Sales Development Technical Development course with our Senior Sales Development Solutions Architect. The goal of this course is to enable you to be more comfortable have technical discussions - specifically when it comes to GitLab’s use cases. Each level of the course is tied to our [levels](https://about.gitlab.com/job-families/marketing/sales-development-representative/#levels) in the BDR and SDR roles.


## Sales Development Resources

| Sales Development Resources    |  Description       |
| :---- | :---- |  
|  [Territory Alignment](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#sdr-team-overview)| *Currently the single source of truth for BDR and SDR/Territory Alignment, Public Sector BDR territory [**here**](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#pubsec-amer-sdrs)* |
|  [Enterprise BDR Outbound Process Framework](https://drive.google.com/drive/search?q=%20Outbound%20Process%20Framework%22%40gitlab.com) | *Outbound process framework for the Enterprise BDR team. Note: These vary by team and geo*|
|  [Sales Development Enablement Videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KrjbznnEEiCtxUfT8-OV6X8)| *Enablement videos and how-tos for BDRs and SDRs* |
|  [Outreach sequence naming convention](https://docs.google.com/spreadsheets/d/12YXwIE126g0rmmNi7QYy2s0SewIV-2JWCr9YUPcqfaQ/edit#gid=0)| * BDRs and SDRs should leverage the naming convention structure and speak to their team about whether the sequence should is approved to be a part of their team's collection of sequences

| Marketing Resources    |  Description       |
| :---- | :---- |  
|  [Marketing Resource Links](https://docs.google.com/spreadsheets/d/1NK_0Lr0gA0kstkzHwtWx8m4n-UwOWWpK3Dbn4SjLu8I/edit?usp=sharing)| *GitLab whitepapers, ebooks, webcasts, analyst reports, and more for Sales & Sales Development education*|
|  [Marketing Events + Gated Content Assets + Webcasts](https://drive.google.com/drive/search?q=%22Events%20Gated%20Content%20Assets%22%20owner:jgragnola%40gitlab.com) | *BDRs and SDRs can use this sheet to refer better understand the marketing assets that are being consumed by prospects. To view the ungated content, click on the link in the Pathfactory or PDF/YouTube columns. Note: Sharing non-gated assets requires manager approval* |
|  [GitLab Buyer Personas](/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/)| *Resource to help GitLab sellers better understand our buyers and audiences*

| Sales Resources    |  Description       |
| :---- | :---- |  
|  [Sales handbook page](/handbook/sales/) | *GitLab Sales team handbook* |
|  [Sales resources page](/handbook/sales/#quick-reference-guide) | *Link to the Sales quick reference guide with links to learn more about the various sales teams & initiatives* |
|  [Weekly sales enablement](/handbook/sales/training/sales-enablement-sessions/) | *These sessions take place every Thursday and BDRs/lSDRs have an open invitation to participate* |
|  [Sales Training handbook page](/handbook/sales/training/) | *Link to GitLab sales training* |
|  [Command of the Message](/handbook/sales/command-of-the-message/) | *"Command of the Message" training and the GitLab value framework* |
|  [Most commonly used sales resources](/handbook/marketing/strategic-marketing/sales-resources/)| *Sales resources page*
|  [Flash Field newsletter](/handbook/sales/field-communications/field-flash-newsletter/)| *Learn more about sales' weekly newsletter*

## Common Sales Development Org Terms and Definitions

| Term | Definition |
| ---- | ---------- |
| Accepted Lead | A lead that an SDR or BDR agrees to work until qualified in or qualified out |
| Account | An organization tracked in salesforce.com. An account can be a prospect, customer, former customer, integrator, reseller, or prospective reseller |
| AM | Account Manager |
| AE | Account Executive |
| APAC | Asia-Pacific |
| BDA | Business Development Associate - focused on sourcing |
| BDR | Business Development Represenative - focused on outbound |
| CAM | Channel Account Manager |
| CS | Customer Success |
| EMEA | Europe, Middle East, and Africa |
| EULA | End User License Agreement |
| High intent | an event, webcast, demo that is a strong indicator of purchase or evaluation |
| Inquiry | an Inbound request or response to an outbound marketing effort |
| [IQM](/handbook/marketing/revenue-marketing/sdr/#qualified-meeting) | Initial Qualifying Meeting |
| LATAM | Latin America (includes all of Central & South America) |
| MQL | Marketo Qualified Lead - an inquiry that has been qualified through systematic means (e.g. through demographic/firmographic/behavior lead scoring) |
| MVC | [Minimal Viable Change](/handbook/product/product-principles/#the-minimal-viable-change-mvc) (not Model View Controller) |
| NORAM | North America |
| Qualified Lead | A lead that a BDR or SDR has qualified, converted to an opportunity and assigned to a Sales Representative (Stage `0-Pending Acceptance`) |
| RD | Regional Director |
| ROW | Rest of World |
| SAL | Strategic Account Leader |
| Sales Admin | Sales Administrator |
| Sales Serve | A sales method where a quota bearing member of the sales team closes the deal |
| [SAO](/handbook/marketing/revenue-marketing/sdr/sales-sdr-alignment/#criteria-for-sales-accepted-opportunity-sao) | Sales Accepted Opportunity - an opportunity Sales agrees to pursue following an Initial Qualifying Meeting |
| SDR | Sales Development Representative - focused on inbound |
| Self Serve | A sales method where a customer purchases online through our web store |
| SLA | Service Level Agreement |
| SQO | Sales Qualified Opportunity |
| TAM | Technical Account Manager |
| TEDD | Technology, Engineering, Development and Design - used to estimate the maximum potential users of GitLab at a company |
| Won Opportunity | Contract signed to Purchase GitLab |

## Other Helpful definitions

**Hyper-personalization** - This is the concept of combining real-time data extracted from multiple sources to create outreach that resonates with prospects on an individual level. The desired outcome is to establish relevance with a prospect as a first step towards a conversation.

**VIP (prospect)** - A Very important top officer, executive buyer, C-level prospect, or important influencer. For these individuals, hyper-personalization is required. Examples: CTO, CIO, CSIO, C-level, IT Business unit leads, VPs, strategic project leaders.

**Influencer (prospect)** - An individual prospect that is suspected to be involved with IT decision-making, tooling, teams, roadmap, strategic projects, and/or budgets. Examples: Director or Manager of DevOps / Engineering / Cloud / Security, Enterprise Architects, IT buyers, SysAdmins, purchasing managers, and product owners.

**User (prospect)** - A prospect that has limited influence within an IT organization. Examples: Developers, Engineers, QA, consultants, and business users.

**Groundswell** - An outbound strategy focused on filling the top of the funnel by generating engagement, opt-ins, MQLs, and uncovering intent signals. This strategy typically incorporates more automation than other more direct outbound prospecting tactics. The strategy should be used with lower-level prospects and lower-tier accounts.

**Snippets** - Content created for BDRS and SDRs to use to create hyper-personalized sequences, one-off emails, or to use for reaching out to prospects via LinkedIn.

**Warm Calling** - The method used to strategically incorporate phone calls and voicemails into an outbound prospecting workflow. The idea is to optimize outbound productivity by only using the phone to call *engaged*, *validated*, and/or *VIP* prospects.


**BDR Team Breakdown**
     *  PubSec
     *  AMER Mid-Market
     *  AMER Large Expand
     *  AMER Large Land East
     *  AMER Large Land West
     *  EMEA Mid-Market
     *  EMEA Large Expand
     *  EMEA Large Land
     *  APAC Large/MM    

## Explaining the Sales Development Organizations Structure

#### Business Development Associate (BDAs) - Sourcing and Learning Focused
**Attributes of team:**
* Willingness to be coached
* Field Marketing Event Support
* Support for vacant territories when needed
* Account and Lead research for BDR teams
* Talent Incubator for SDR Teams

#### Sales Development Representatives (SDRs) - Inbound Focused
**Attributes of team:**
* Fast Response Time
* Global coverage
* Aligned w Marketing re: SLAs and Feedback on campaigns
* Defined and prescriptive inbound processes
* Round Robin assignment rules
* Talent Incubator for BDR Teams


#### Business Development Representatives (BDRs) - Outbound focused
**Attributes of team:**
* Alignment with SALs/AE & Sales leadership
* Alignment with Field Marketing + ABM
* Strategic account planning and research
* Targeted and creative messaging
* Talent Incubator for Sales Teams

## Segmentation of BDR Team
The Business Development team aligns to the [Commercial](/handbook/sales/commercial/),Large, and PubSec sales teams. These teams are broken down into three segments: [Large, Mid-Market and SMB](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation) which are based on the total employee count of the Global account. *Note: The commercial sales team includes both Mid-Market and SMB. This segmentation allows BDRs and Sales to be focused in their approach and messaging. The segments are aligned to a region/vertical and then divided one step further via territories in the regions. Our single source of truth for determining the number of employees is  Zoominfo.
* [Sales segmentation](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation)
* Sales territories(Link to Come)
* [Determining if a lead is in your territory](/handbook/sales/field-operations/gtm-resources/rules-of-engagement/)
* [BDR and Sales alignment](/handbook/marketing/revenue-marketing/sdr/sales-sdr-alignment/) - working with your SAL or AE!

## Sales Development Organization Standards

We hold our Sales Development Org accountable to three pillars:

- **Uphold daily activity metrics**, in terms of sequencing new leads, researching outbound accounts and actioning sequence steps in time.

    - MQLs to be sequenced within expected [activity metrics](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#lead-views).

    - Work lead records within Salesforce by leveraging [relevant sequence libraries in Outreach](https://about.gitlab.com/handbook/marketing/marketing-operations/outreach/#collections).


    - Use our Business Intelligence platforms like ZoomInfo or LinkedIn to verify the validity of inbound lead datapoints. Take appropriate actions to clean up and assure accuracy and consistency of SFDC/Outreach data. Add any additional information sourced from your research, or correct wrong data altogether.

    - Maintain a sense of ownership of data integrity in [Salesforce](https://about.gitlab.com/handbook/sales/field-operations/sfdc/) and [Outreach](https://about.gitlab.com/handbook/marketing/marketing-operations/outreach/#outreach-merge-and-delete). Clean up and assure accuracy and consistency of data. Add any additional information gathered from our Business Intelligence platforms, ZoomInfo or LinkedIn for example. about a LEAD, CONTACT, or ACCOUNT from our data source Zoominfo where you can into SFDC.

    - [Sequence steps](https://about.gitlab.com/handbook/marketing/marketing-operations/outreach/#activity) to be actioned within the same day of them being due. Move steps to a further date only because of national holidays affecting outreach (ie. Christmas day)

    - Maintain SFDC data integrity by [following the outbound workflow](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#outbound-workflow) in researching and prospecting accounts.


    - Maintain [cross-functional relationships](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#working-with-sales-1) in terms of outbound account planning. Attend each initial qualifying meeting (IQM) with the AE/SAL you work with. Document accurate up-to-date notes in SFDC, including adding the Contacts you have been in communication with and attaching all corresponding connects and attempts to the Contact record. Communicating with the AE/SAL teams before and after the meeting to ensure accurate briefs are provided and tangible mutually agreed-upon next steps are set.

- **Displaying business and sales accumen** in terms of appropriate use of sales methodologies and strategic preparation prior to each touchpoint with prospects.

    - Display business accumen and sales skills in personalizing inbound/outbound leads, in accordance with our [CoM email writing principles](https://docs.google.com/document/d/1-DF6bEtS9QF9idqBcK77RiLL04CKiFMuc0LDEM5N6RA/edit).

    - Display business accumen and sales skills is preparing for cold-calls, in accordance with our [CoM cold-calling principles](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#cold-calling-checklist)

    - Display business accumen and sales skills is preparing for scheduled discovery calls, in accordance with our [CoM sales training](https://about.gitlab.com/handbook/sales/command-of-the-message/)

    - Outbound accounts to be added in a weekly cadence, according with the expectations set within each regional team, in terms of outbound cycle (monthly, weekly) and volume of accounts.

- **Maintain cross-functional relationships**, with the relevant stakeholders of your territory or segment, while enforcing the principles outlined above.

    - [Collaborate with the Sales team](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#working-with-sales) in terms of [account planning](https://about.gitlab.com/handbook/sales/account-planning/)

    - [Collaborate with the Field Marketing team](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#event-promotion-and-follow-up-assistance) in terms of event outreach


## BDR and SDR Compensation and Quota

Quota is made up of the following depending on [sales segment](/handbook/marketing/revenue-marketing/sdr/#segmentation):
- [Sales Accepted Opportunities (SAOs)](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/sales-sdr-alignment/#goals--targets)
- [ARR](/handbook/sales/sales-term-glossary/arr-in-practice) pipeline component  
- 2-way communication must be documented on the Contact in the Opportunity to receive credit. Opportunities missing this documentation will not be considered for compensation and will not retire quota.
- For BDRs and SDRs who have SAO/ARR goals, compensation is based on the SAO attainment. ARR attainment is a qualifier for accelerator payments. Our aim is to land net new logos and expand to new customer business units, SAOs being our quantity metric and ARR our quality metric.  Our mission is to create good qualified pipeline for the company.
- BDRs and SDRs are not responsible for uptiering customer accounts from a paid lower tier to a higher tier, ie. from Premium to Ultimate, this is the responsibility of sales.

#### Sales Development Org Compensation Breakdown
**Team Member OTE (SDR/BDR MM, ENT Land & Pub Sec)**
* 70% Base Pay
* 30% Variable Pay:
    * 70% Individual Quota:
        * No floor or ceiling
        * Accelerator x 1.5 after meeting 100% of target
    * 30% Team Quota:    
        * Floor of 75%
        * Ceiling of 150%
        * Accelerator x 1.25 from 100% to 150%

**Team Member OTE (ENT Expand)**
* 70% Base Pay
* 30% Variable Pay:
    * 50% Individual Quota:
        * No floor or ceiling
        * Accelerator x 1.5 after meeting 100% of target
    * 25% Net ARR:    
        * No Floor
        * Ceiling of 200%
        * No Accelerator
    * 25% Team Quota:    
        * Floor of 75%
        * Ceiling of 150%
        * Accelerator x 1.25 from 100% to 150%

## Activity & Results Metrics

While the below measurements do not impact your quota attainment, they are monitored by Sales Dev leadership.

**Results**
  * ARR won from opportunities BDR sources

**Activity**
  * Number of opportunities created
  * Number of calls made
  * Number of personalized emails sent
  * LinkedIn InMails
  * Drift Chats
  * Number of leads accepted and worked


  **Daily outbound metrics**
    * We aim for 45 touchpoints per day using the various methods above.  This is a broad expectation and may vary per team given the segment, functionality and strategy of the team manager.

## Flexible working
At GitLab we have flexible working hours. This means that you can organize your working day as you like and are not expected to arrive and leave at a set time every day, but we do expect MQL’s to be followed up within 1 hour (during business hours) so communication with your manager will be necessary if you plan to be out for an extended period of time. Managers will provide direction on the routing of leads during that time.

With a prospect-facing role in Sales Development, do keep in mind _when_ you are calling and emailing prospects:
 * The best times to call are early and late in the business day, so these may be best blocked for this activity.
 * Outreach allows you to schedule emails at set times. For outbound prospecting, the most effective emails are those that come in the early morning and early evening, so you can take advantage of this feature. On the other hand, inbound MQL’s *must* be handled regularly throughout your day as they come in.
 * Lunchtimes are good for outreach as most prospects have meeting blocks for three hours in the morning and then two hours mid-afternoon.
 * The above blocks you can use for account research and planning, customer meetings, or meetings with your sales reps, manager, peers, and teams.
 * Although you are not expected to start work and end work at the same time every day, please do keep in mind the normal business hours of your prospects, and make sure to schedule your own working hours to ensure you maximize opportunities.

In short, working at GitLab means that if you start the day early you can take a long lunch break to hit the gym or go grocery shopping in the middle of the day when it’s quiet, yet you can still do a full day’s work around that and don’t need to be accountable all the time, as long as you are available to call prospects and be available for prospect meetings during normal working hours for your region/territory.


## Sales Development Tools

You can find a detailed breakdown of the Sales Development Organizations tools and best practices on [this page of the handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/sales-development-tools/)

## BDR and SDR Inbound Workflow & Process
As a BDR or SDR, you will be focused on leads - both inbound and outbound. At the highest level, a lead is a person who shows interest in GitLab through inbound lead generation tactics or through outbound prospecting.


### Working Inbound Leads

#### What is an MQL?
The GitLab marketing team uses digital channels - social media, email, mobile/web apps, search engines, websites, etc - as well as in-person marketing activities to meet potential buyers where they are. When people interact with GitLab, we use lead scoring to assign a numerical value, or points, to each of these leads based on their actions and the information we have about them. Once a lead reaches 100 points, they are considered a [Marketing Qualified Lead](/handbook/marketing/marketing-operations/marketo/#mql-and-lead-scoring) or MQL.


#### Inbound Process
SDRs are responsible for following up with MQLs by reviewing their information, reaching out, and working with them to understand their goals, needs, and problems. BDRs have the same responsibility for MQL’s that come from one of their “actively working” accounts. Once you have that information, you can use our [qualification criteria](/handbook/sales/field-operations/gtm-resources/) to determine if this is someone who has strong potential to purchase our product and therefore should be connected with sales for next steps. As you are the connection between Marketing and Sales you want to make sure every lead you pass to the Sales team is as qualified as possible.
Note: SDR routing is determined by Region and Level. The three SDR Routing Levels are onboarding, ramped, and expert. During onboarding, you'll receive SMB and Mid-Market MQLS with a goal limit of 30 per day. At a ramped leve, you'll receive SMB, Mid-Market and ENT First Order MQLs with a goal limit of 50 per day. And at Expert level you'll receive MQLs from all segments including ENT Expand with a goal limit of 75 per day. Expert Level SDRs also are responsible for managing all Drift conversations.

These MQLs will show up in your S1 or B1 lead and contact views in Salesforce. The other views, listed below, allow you to see your leads in a categorized way to simplify your workflow. Leads are routed to you and flow into your views via the tool [LeanData](/handbook/marketing/marketing-operations/leandata/) which takes each lead through a series of conditional questions to ensure it goes to the right person. Even though all SDRs leverage the same views, they will only show you leads that have been specifically routed to you. You will be responsible for following up with all of the leads in your MQL views by sequencing them using [Outreach.io](/handbook/marketing/marketing-operations/outreach/). Once sequenced, their lead status will change and they will move from your MQL views allowing you to have an empty view. Managers monitor all views to ensure they are cleared out. If you choose not to work a lead please unqualify it and state your reasons in the lead record.

## Lead and Contact Views

#### SDR Lead Views

* **S1 View** - [MQL’s](https://about.gitlab.com/handbook/marketing/marketing-operations/marketo/#mql-and-lead-scoring) and Hot leads that require quick follow up (i.e event)s, and Drift conversation leads for tenured SDRs
* **S2 View** - Only leads that are active in a HT touch sequence and have a phone number
* **S3 View** - Qualifying leads. These are leads that you are actively qualifying in a back and forth conversation either by email or through phone calls.  Each lead needs to either be active in a follow-up sequence, have an active task, or have a future meeting scheduled which can be seen in a future “last activity” date.

#### BDR Lead Views

* **B1 View** - [MQL’s](https://about.gitlab.com/handbook/marketing/marketing-operations/marketo/#mql-and-lead-scoring) and Hot leads that require quick follow up
* **B2 View** - Leads associated with “Actively Working” accounts where you are the BDR Assigned
* **B3 View** - Needs to be sequenced. They’re leads you chose to move into your name but they have not yet been sequenced
* **B4 View** - Active HT sequenced leads that have a phone number - to help with call downs
* **B5 View** - Qualifying leads. These are leads that you are actively qualifying in a back and forth conversation either by email or through phone calls. Each lead needs to either be active in a follow up sequence, have an active task or have a future meeting scheduled which can be seen in a future “last activity” date


#### BDR Contact Views

**Contact ownership is based on the BDR Assigned and Account owner fields. If you are the BDR Assigned on the account, you will be the owner of all contacts associated with that account. If there is no BDR Assigned, the account owner will be the owner of the contacts.**

* **B1 View** - Includes MQL’s, Hot contacts that require quick follow up
* **B2 View** - Contacts related to Actively working accounts that you can choose to sequence - *give 48 hrs
* **B3 View** - Active HT sequenced leads that have a phone number - to help with call downs
* **B4 View** - Qualifying leads. Contacts that you are actively qualifying in a back and forth conversation either on email or through phone calls. Each contact in this status needs to either be active in a follow up sequence, have an active task or have a future meeting scheduled which can be seen in a future “last activity” date.

## SDR/BDR ROE and Inbound Lead Management

### Definitions

* **Rules of Engagement (RoE):** Defines who has ownership over a lead, who gets credit for the SAO, and how the SDRs and BDRs can work together to achieve goals.

* **BDR Prospecting Status:** Salesforce status that will includ Queued, Actively Working, Worked in FY, and Restricted

* **Actively Working” Status:** An account researched and chosen by the BDR and/or SAL- based on alignment to ICP, news insights, company initiative, intent, ABM list etc. This is an account that the BDR will proactively be performing strategic outreach to. There is no limit to how long an account can stay in “Actively working” status as long as outreach is continuing to be made.
BDRs will work these leads.

* **Requirements to remain in Actively Working status:** All  “BDR comments” fields will need to be populated within 10 days of moving into “Actively Working” status and a minimum of 5 people will need to be added to sequences within 15 days of the “Actively Working” timestamp. Accounts will move back to queued should criteria not be met.
    * MM has a limit of 150 accounts in “Actively Working” status, Large has a limit of 100.

* **Queued status:** Accounts with this BDR prospecting status are waiting to be moved into Actively Working
    * SDRs will work these MQLs.

* **Worked in FY status:** Accounts with this BDR prospecting status are actively worked but has not had prospects active in sequence in the last 30 days. Being moved to “Worked in FY”  indicates that they have gone through being an Actively Working” account this FY. An account in this status can be moved back into “Actively Working” working status later that year if desired.
    * SDR will work MQLs for that account.

* **Restricted status:** SAL indicated that this account cannot be worked by BDR. BDRs should be the only ones moving the BDR Prospecting Status. So if you’re asked to move an account into restricted status, please make sure you note the reason for the restriction in the BDR comments section under “BDR account research”.
    * SDR will reroute to BDR Assigned for these accounts

## Rules of Engagement Quick Guide

* **Is the MQL from an Existing Account?**
* **No:** It is worked by the SDR team
* **Yes:** Check the BDR Prospecting Status
    * **Is the Account in “Actively Working” Status?**
        * **No:** It is worked by the SDR team
        * **Yes:** It is worked by the BDR team
    * **Is the Account in “Queued” Status?**
        * **Yes:** It is worked by the SDR team
    * **Is the Account in “Worked in FY” Status?**
        * **Yes:** It is worked by the SDR team
    * **Is the Account in “Restricted” Status?**
        * **Yes:** It should be routed to the BDR assigned. The BDR assigned will chatter the SAL about whether they'd like them to reach out or whether they would like to own that responsibility. They'll have a certain window to respond (likely 24-48 hours depending on what JB says).
            * If they reply back that the BDR can work, the BDR will continue with the prospect as a lead record and take action.
            * If they reply back that they want to be responsible, the BDR will convert the lead so that it becomes a contact.

## Inbound Lead Management

**Begin with Enriching and using Find Duplicates**
1. On SFDC lead record, you will be using the  **Find Duplicates** button and LinkedIn and follow the workflow below.
2. First Click **“LinkedIn”** to see if you can enrich any title or company info which will help ensure correct search of duplicates and inform the collection you’ll use in Outreach.
3. Checked **“LeanData Matched Account”** and **ZoomInfo** sections:
    *   For Commercial leads (SMB and MM), if the existing SFDC account type equals "Customer", press 'Convert to Account' and convert the lead to a contact for that account. In case of a Contact Request lead source, please also chatter the Account Owner and inform them of the context of the message for them to action.
    *   If an existing SFDC Large/Enterprise Customer account already exists, verify that the SFDC lead/prospect is not part of the existing GitLab subscription before working the lead by sending a short snippet like, "Hi XYZ, Thanks for reaching out. ABC company current has a GitLab subscription, are you currently using a GitLab license and if so, what version?". If they are, please follow the "Convert to Account" instructions above.
    *   Read the ZoomInfo description to check for PubSec indicators which will  include word like the following in the description: County, City Of, State, University, College, Defense, Intelligence, Agency, Mission, Mission Critical, Mission Support, Speed to Mission, System Integrator, Contract Vehicle, Government Bid, Government Contract, Civilian, or Task Order.  **Canadian government leads get routed to the AMER commercial team.**
4. Press **Find Duplicates**
    *   SDRs/BDRs should use this screen to check to see if 1) the matched account is an “Actively Working” account that would cause the lead to need to be re-route to the BDR assigned or 2) whether there is a duplicate record that needs to be merged or 3) whether there is another lead at that company active in sequence.
        *   The lead owner will make sure to use search techniques that allow for the most accurate results. On the Find Duplicates screen, they will need to scroll to the account section to see if the BDR prospecting status is “Actively Working”. If so, they will need to transfer to BDR.
        *   If it is not an Actively Working account, then the next step is to scroll to the top to see if there are leads or contacts that are a match. If so, then our merging process here will need to be followed.
        *   If there is another lead at the same company with a Lead Status of MQL, ownership is determined by the lead whose MQL Date is the earliest. If there is another lead at the same company with a Lead Status of Accepted or Qualifying, pass the MQL to the owner of the accepted/qualifying lead.
    *  Should a BDR receive a lead not associated with an Actively Working account, they are responsible for assigning the lead back to the SDR who assigned it to them. If it came from LeanData, they should chatter mktgops support on the record so that it can be routed correctly.

5. Drift leads associated with **“Actively Working”** accounts
    *  SDR will be routed the lead and will need to quickly offer a meeting with the assigned BDR. From there they will move the lead into qualifying status, transfer ownership to the BDR and click to send notification.


**Outreach & Marketo**

If the lead is yours to work based on all of the above, sequence them in Outreach using the FY23 inbound sequence that correlates with the Last Interesting Moment. You can also look at the initial source and Marketo insights to get a holistic view of what a lead is looking into. There is an Outreach collection of sequences for inbound leads for each region. These collections contain a primary sequence for nearly every inbound lead scenario.
* **Primary Sequence**: a sequence created by SDR leadership that should be used by all reps to follow up with inbound leads
*  [**Sequence collection**](https://app1a.outreach.io/content-collections): a group of sequences compiled by region
    * [FY23 Inbound High Touch Collection](https://app1a.outreach.io/sequences?direction=desc&order=recent&content_category_id%5B%5D=44)
    * [FY23 Inbound Low Touch Collection](https://app1a.outreach.io/sequences?direction=desc&order=recent&content_category_id%5B%5D=45)
    * [FY23 Inbound Languages Collection](https://app1a.outreach.io/sequences?direction=desc&order=recent&content_category_id%5B%5D=46)
*  **Last Interesting Moment**: data pulled in from our marketing automation software, [Marketo](/handbook/marketing/marketing-operations/marketo/), that tells you the last action a lead took.
*  [**Initial source**](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#initial-source): first known action someone took when they entered our database
*  [**Marketo Sales Insights (MSI)**](/handbook/marketing/marketing-operations/marketo/#marketo-sales-insight): a section on the lead/contact/account in Salesforce that shows you compiled data around actions a lead/contact has taken

#### High touch and Low touch sequences:

A high touch sequence should be used for higher-quality leads. High touch sequences require you to add in more personalization and have more touch points across a longer period of time, including phone calls and LinkedIn Connect requests. Low touch sequences are typically automated and run for a shorter period of time. We define higher quality leads based on the volume of inbound you have to manage as well as whether the lead has has 1-2 of the following for MM/Large accounts:
* Valid company email domain or company name
* Phone number
* Title related to our buying/influencer personas
* Demandbase intent
* Set up for company/team use checkbox equals true
* Account has X # of Developers (MM/Large nuances)
* LinkedIn profile
* SMB  lead may need to have  2-3 of the above
* Different teams may have nuances so please check in with your manager for their feedback.


#### Questions about a lead?

* If you feel you have been misrouted a lead, chatter mktgops-support on the lead record. They will reassign.
* If you have a question about the behavior of a lead or any other operational issue, you can use the slack channel #mktgops
* If lead is associated with an account where there are duplicates, chatter sales support on the account record with links to duplicate account(s) and ask them to merge. The chatter should be on the record that seems to be the most active account.
* For contact requests received requesting event sponsorship, please change ownership to GitLab Evangelist in SFDC & be sure to "check the box" to send a notification.
* If a lead only has CI minutes, but no paid subsriptions they are considered a prospect and not a customer.


**Technical questions from leads?**
* [Docs](https://docs.gitlab.com/)
* Slack channels
    * #questions
    * #support_self-managed
    * #support_gitlab-com

#### Dealing with spam and or unneeded leads

Every so often you may be assigned a lead that has no value to GitLab and is not worth keeping within our database. Qualities that define these types of leads include:
  * Lead info is an incoherent, jumbled combination of characters with no meaning
  * Lead info is an obvious forgery, e.g. The odds of someone being named `Batman Batman` are very low
  * Email supplied is from a temporary email domain that cannot be contacted after a certain time limit due to email self-termination

When you come across leads that fall into this category, it is best to remove them from our Salesforce database entirely. The process to remove leads is to add the lead to the [SPAM DELETION Salesforce campaign](https://gitlab.my.salesforce.com/7014M000001dphz). A few details about this process:
  * Only add leads to this campaign if you are 100% certain they are worth removing from the database
  * MktgOps cannot recover leads removed through this process. You should consider the removal permanent, so be careful
  * The `campaign member status` used when adding to the campaign does not matter, only `campaign membership` matters for this process
  * The removal process, which is completed via `Marketo`, runs once a day at 12:05am PDT
  * If you require leads to be cleared out before the scheduled time, please ask in the `mktgops` Slack channel. Please keep unscheduled requests to a minimum
  * If you spot a number of leads with the same domain, please [create an issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new) with Marketing Ops to have that email domain added to a block list. You can still follow the steps above to delete them.

#### Lead sequencing and dispositioning through Outreach

Lead/contact statuses allow anyone in Salesforce to understand where a lead is at in their journey. The automation mentioned below is all done through an Outreach.io trigger.

* Once a lead has been placed in an Outreach sequence, the lead status will automatically change from MQL, Inquiry, or Raw to Accepted marking that you are actively working on this lead.
* When a lead responds to you via email, their status will again automatically change. This time it will change from Accepted to Qualifying. You will manage these leads from your S3 and B5 lead views.
    * If you are not working on qualifying this lead further, you will need to manually change the status to Nurture so that this lead is back in Marketing nurture and isn’t stuck in your My Qualifying view. If you have spoken to the lead by phone and are qualifying them, you need to manually change the status to from Accepted to Qualifying
* When looking at your qualifying view please sequence leads that have no recent last activity + no active tasks + are not actively being sequenced into one of our follow up sequences that have the "Follow up Ruleset Sequences". You can read more about our [rulesets here](https://about.gitlab.com/handbook/marketing/marketing-operations/outreach/#rulesets)
* If a lead finishes an Outreach sequence without responding, the lead status will automatically change to unresponsive or nurture in seven days if there is still no response.
* If a lead responds, the BDR/SDR is to schedule a call/meeting using [Outreach’s meetings feature.](https://about.gitlab.com/handbook/marketing/marketing-operations/outreach/#outreach-meetings)
* The only time you will need to manually change lead status outside of what is mentioned in the previous bullets is if you for some reason don't use an Outreach sequence to reach out to someone or if you need to unqualify a lead for bad data etc.
* If you check the `Inactive lead` or `Inactive contact` checkbox, signifying that this person no longer works at the company, any running sequence will automatically be marked as finished.

## Process for scheduling an IQM with AE/SAL

Whether it be from inbound follow up or outbound prospecting, you’re going to be engaging with prospects who are directly involved in a project or team related to the potential purchase of GitLab within a buying group, either as an evaluator, decision-maker, technical buyer, or influencer. During those interactions, you’ll be looking to gather information related to the SAO qualification criteria.


### Qualification Criteria and SAOs

Qualification criteria is a minimum set of characteristics that a lead must have in order to be passed to sales and become a Sales Accepted Opportunity (SAO). You will work to connect with leads that you get a response from to obtain this information while assisting them with questions or walking them through how GitLab might be able to help with their current needs. The qualification criteria listed in the linked handbook page below aligns to the 'Qualification Questions' sections on the LEAD, CONTACT, and OPPORTUNITY object in Salesforce. In order to obtain an SAO, you will need to have the 'required' information filled out on the opportunity including documented 2-way communication on the Contacts in the Opportunity.  
*  [Qualification criteria needed](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#criteria-for-sales-accepted-opportunity-sao)

**When do I create an Opportunity?**
If you are scheduling time with an AE/SAL to speak to the prospect based on a qualifying conversation you have had with the prospect, an opportunity needs to be created and placed in stage 0. On the opportunity, you should be trying to populate as many of the "required qualification" fields as possible.

-  **Who owns the opportunity at this point?**
     At this point, the opportunity will be in your ownership but you can not have yourself listed in the Business Development Representative or Sales Development Representative field. There is a validation rule that will not allow the opp owner and the SDR/BDR representative field to be the same member.

**When do I transfer the opportunity to sales?**
When you have entered the qualification criteria onto the opportunity, and have received calendar confirmation from all parties for the intro call with sales, you will change ownership to the AE/SAL. After you have saved opp owner, you will add yourself to the BDR/SDR field. The opportunity will remain in stage 0.

**When will it be credited as an SAO?**
After the SAL/AE meets with the prospect to verify and supplement qualification criteria, they will move the opportunity into Stage 1. The AE/SAL is expected to move the opportunity into the correct stage within 48 hours of the meeting reflected in the "Next Step" date field.


* [ How to create an opportunity](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#creating-a-new-business-opportunity-from-lead-record)

Before scheduling the meeting with the AE/SAL, you will need to verify the sales organization RoE

1. On ZoomInfo or other sources, verify the [parent/child segmentation:](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/rules-of-engagement/). All accounts in a hierarchy will adopt the MAX segmentation of any account in the hierarchy and ownership between segments is determined accordingly.
2. On ZoomInfo or other sources, verify the HQ of the company or the ultimate user. Unless the account is Named, lead ownership is determined based on the HQ of a company, regardless of the lead's location geographically.
3. If there's an account already assigned to a different segment, please do not immediately pass leads that your research says should be for another segment. Instead, please chatter ASM and the current account owner with your data. If there is a consensus, please then tag Sales Support for the reassignment. Please be mindful of discrepancies between our SSOT and LinkedIn. There are many cases where LI employee count does not indicate the reality of their employee size. For example, in companies with commonly used names, LI can overstate their employee count. On the other hand, LI can understate the employee count of companies that do not have a very strong tech presence.


## BDR Outbound Process

BDR outbound lead generation is done by prospecting to companies and individuals who could be a great fit for our product. Prospecting is the process of finding and developing new business through searching for potential customers with the end goal of moving these people through the sales funnel until they eventually convert into customers.

BDRs will work closely with their dedicated SAL or AE to choose which accounts move into “Actively Working”. Together they will build a strategy for those companies. It is crucial that outreach is very intentional and strategic. When reaching out, we want BDRs to offer value and become a trusted advisor, ensuring a positive impression is left whether there is current demand or not.

You do not have to wait until the end of the month or quarter to change the BDR prospecting status, you just have to make sure you stay under the limit for your segment and move the account to either “Queued” or “Worked in FY” based on criteria.

BDR Account views are to help with territory organization. It is very important for BDRs and their manager to update the BDR assigned field for their territory. Contacts ownership will be based on the BDR Assigned to the account. BDRs will be responsible for contact follow up should they MQL. BDRs will also be responsible for any leads that MQL when they’re related to accounts where they are the BDR Assigned and the account is in “Actively Working” status.

**Account views**

* **B1 View**- All accounts in territory, (clone) - This view will need to be cloned to be customized to the accounts in the territory the BDR is aligned to. It will allow the BDR to mass update the BDR Assigned field. The BDR manager should work with the BDR on the fields needed in the logic.
* **B2 View**- My BDR Assigned accounts, (clone) - This view will need to be cloned to be customized to accounts where you are the BDR assigned. Once set up, it will allow the BDR to mass update the 'BDR Prospecting Status' field to show which ones they will be "Actively Working'. They can also use this view to move into other statuses.
* **B3 View**- My Actively Working accounts, clone - This view will need to be cloned to be customized to show “Actively Working” accounts where you are the BDR assigned.

**Before moving an account into “Actively Working Status”,** the BDR will do a search using this [Demandbase list](https://web.demandbase.com/o/d/p/l/261084/l) to make sure there have been no MQLs from that account within the past 30 days. Exception to the process, the BDR can see that the MQL was put into a low touch sequence. If so, can ask SAL to request via chatter on the lead record that this account be placed into “Actively Working” so that BDR can reach out to others at the account.

Accounts should not be moved to “Actively Working” until the BDR is ready to begin account research, strategy, and sequencing leads. There will be a set # of days that a BDR has to complete these actions before the account moves back to a BDR prospecting status of “Queued”

Ideas for account research and strategy: "coming soon"

* Example: You can focus on Closed Lost Opportunities once they are older than 30 days, in line with the Sales Handbook [Opportunity Stages](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#opportunity-stages)

### Best Practices for Outbound BDRs

### Cold-Calling Checklist

Our cold-calling best practices typically consist of 4 elements. A pattern interrupt, an elevator pitch and, if required, objection/trap-setting questions and Up-Front Contracts.

To be effective, these need to be customized to the individuals we call as per the logic below:
* Different Geographical Business Cultures
* Personality Type as per the DISC model
* Individual's Role and Responsibilities
* Business’ Needs and Objectives

The main documentation may be found [here](https://docs.google.com/document/d/1D3iV_WW5fRidRN5H8-3SZVAAr3ffEvjxUC6cW5SFXDM/edit)

### Up-Front Contract

Typically used for scheduled calls and Up-Front Contracts. UFCs can also be used as a defense mechanism when cold calling does not go as planned. For cold-calling, UFCs are used when the prospect can commit to 15’ at the time of the call. If they commit to a 15’ for a later time, UFCs are used to open the call and set the agenda then.

Explore the main documentation [here](https://docs.google.com/document/d/1Y7qEq8g3eHh5-oagERGvNmatiOV3JXi9Tw46SKWwpNM/edit)

### Email Writing Cheat Sheet

The table below shows the Command of the Message Email Writing Checklist.

It aims to outline the structure of the emails we write to prospects. Emails should be written in each person’s language and tone and make sure to include the CoM frameworks as per the outline below. You can find additional resources [here](https://docs.google.com/document/d/1-DF6bEtS9QF9idqBcK77RiLL04CKiFMuc0LDEM5N6RA/edit)


| Subject Line                                  |                                                                                                                                                                                                                                                                                                      |
|-----------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Personalized                                  | Quote Familiar Names: Use Prospect’s name, Company name, or colleague’s name. Relate to their situation: Use recent company initiatives, technologies they use, Projects they have planned etc.                                                                                                      |
| Curiosity and Urgency                         | Provide an image of their future situation: Guide the reader to imagine how their situation could change. Use compelling events to give a clear image of where they are currently.                                                                                                                            |
| CoM                                           | Proof Points: Quote Case Studies or other customers. How We Do it Better: Make use of our defensible differentiators.                                                                                                                                                                                |
| Opening Line                                  |                                                                                                                                                                                                                                                                                                      |
| Avoid cliches                                 | Stand out: Avoid losing email real-estate with cliche phrases like “just following up” or “hope all is well”.  Brand yourself: Demonstrate that you have an understanding of the reader’s current situation and a clear idea of his required solutions.                                                   |
| CoM                                           | Before Scenarios: Paint a clear image of their current situation and how that is causing them professional or personal pain. Ater Scenarios/PBOs: Tie the current situation with a clear image of a future situation that’s beneficial for the business and the individual.                          |
| Main Body                                     |                                                                                                                                                                                                                                                                                                      |
| Addressing Questions and Information provided | No Free Consulting: Answer questions just enough to give a narrative to your email and tie into the CTA. No Transactional Answers: Don’t make the reader feel like he’s interacting with an online form, a robot, or a sleazy salesman that doesn’t care.                                             |
| CoM                                           | Discovery Questions: determine which discovery questions tie into their current situation, questions asked or information provided. Trap-Setting Questions: if competitor technology or objections come up, use trap-setting questions to guide the reader into understanding our differentiators.   |
| CTA                                           |                                                                                                                                                                                                                                                                                                      |
| Clear Next Step, Agenda and benefit           | Valuable: phrase your CTA in a way that’s clearly valuable to the reader and his specific situation. Defined: outline what will happen in the next step and how long will it take                                                                                                                    |
| CoM                                           | Measurable: present metrics or PBOs that will be covered in the next step                                                                                                                                                                                                                            |


### Database Segmentation
The SFDC prospect database is split into five segments: Core users, free GitLab.com users, trial prospects, current customer "leads" and other prospects. We have checkboxes for these, most of which will automatically populate. **However**, the `Core User` checkbox in Leads and Contacts needs to be checked by the BDR/SDR or Sales rep once the prospect has confirmed *in writing or on the phone* that they are using Core as we cannot rely on our product analytics alone as it does not cover all users.

### Outbound Workflow
**IMPORTANT**: EMEA reps, old leads/contacts for EU nations can only be called or emailed if they were in our system from May 2018 or later. Any lead/contact that was brought into our system prior would need to optin to communication. Any new leads/contacts can only be called or emailed if they have been brought in from ZoomInfo. You can also leverage LinkedIn Sales Navigator.

The [BDR outbound framework](https://drive.google.com/drive/search?q=%20Outbound%20Process%20Framework%22%40gitlab.com) (sample is linked) will walk you through both strategy and execution of outbound prospecting.

When creating new prospects in SFDC these should be added as Leads to the system. New Leads can for example be prospects you are talking to on LinkedIn, prospects you are introduced to by someone at GitLab, or by other Leads or Contacts in SFDC.
Everyone you are contacting should exist on SFDC so that you, your manager, and the Account owner have a full picture of who you are prospecting into. LinkedIn conversations can be sent to SFDC using the "Copy to CRM" feature within the LinkedIn Sales Navigator chat platform.

If you have a list you would like to import to SFDC please follow this [List Import](https://about.gitlab.com/handbook/marketing/marketing-operations/list-import/) process on our Marketing Operations page which outlines how to import CSVs and ZoomInfo Prospects into the system.

### Outbound Messaging

[**Outreach Sequence Library**](https://app1a.outreach.io/sequences)

**Persona-based Messaging:** *Used for focusing on specific titles or personas*
 - [VP, App Dev (Erin)](https://app1a.outreach.io/sequences/6397)
 - [Director, App Dev (Dakota)](https://app1a.outreach.io/sequences/6392)
 - [Manager, App Dev (Alex)](https://app1a.outreach.io/sequences/5983)
 - [CISO (Skyler)](https://app1a.outreach.io/sequences/6426)
 - [Snippets](https://app1a.outreach.io/snippets)

**Business Resilience Messaging** *Revamped value driver messaging for companies impacted by the pandemic*
 - [Deliver Better Products Faster](https://app1a.outreach.io/sequences/5665)
 - [Increase Operational Efficiencies](https://app1a.outreach.io/sequences/5678)
 - [Reduce Security & Compliance Risk](https://app1a.outreach.io/sequences/5746)

**Value Driver Messaging:** *Developed to align to the [GitLab value framework](https://about.gitlab.com/handbook/sales/command-of-the-message/#customer-value-drivers)*
 - [Deliver Better Products Faster](https://app1a.outreach.io/sequences/5815)
 - [Increase Operational Efficiencies](https://app1a.outreach.io/sequences/5816)
 - [Reduce Security & Compliance Risk](https://app1a.outreach.io/sequences/5817)

**Situational Snippets** *Developed to help BDRs and SDRs respond to common prospect responses*
 - [Objection Snippets](https://app1a.outreach.io/snippets?direction=asc&order=owner&tags%5B%5D=objection)
 - [Support Snippets](https://app1a.outreach.io/snippets?direction=asc&order=owner&tags%5B%5D=Support)

## RoE Common Questions

**Q:** Should BDRs flag duplicate accounts?
**A:** Yes. However, they do not have the ability to merge it themselves so should chatter @Sales Support to do it.

**Q:** How do we resolve a dispute over SAO credit?:
**A:** SDR and BDR first try to talk through a solution
If no agreement:
Managers will determine a solution
If no agreement between the managers:
Escalate to Senior Leadership
Double credit nor double compensation will be given

**Q:** Who will be handling inquiries from an SDR/BDR standpoint?
**A:** Inquiries will go to an open queue. SDR’s will not work them. We want to give marketing time to progress these to MQLs.
**A:** BDR’s can work leads in all statuses if they are related to their “Actively working” accounts. Outreach to these leads would be part of their account strategy.

**Q:** What if a prospect comes back directly to the BDR much delayed, when the account is not marked as “Actively Working” anymore, but resulting from personalized messaging?
**A:** Yes, if they email, reply via LI, or call the BDR directly, the BDR will need to check that the lead is in queue ownership. If so, they must move the account back to “Actively Working”, so that the lead can be put into their ownership.

## Event Promotion and Follow Up Assistance

The Sales Development Organization will assist the Field Marketing and Corporate Marketing teams with campaign and event promotion and follow up. Learn more about [Field](/handbook/marketing/field-marketing/) and [Corporate Marketing](/handbook/marketing/corporate-marketing/) via their linked handbook pages.
When these teams begin preparing for an event, they will create an issue using an issue template. Within that template is a section titled ‘Outreach and Follow Up’ for the Sales Development Organization to understand what is needed from them for each event.
*  [Field Marketing SDR Support Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=ISR_SDR_FMTemplate)
*  [Corporate Event SDR Support Template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/Event-SDR-Support-Template.md)

### Approvals Required
The VP of Integrated Marketing & the AVP of SDR require they sign off on issues before BDRs/SDRs are allowed to work on the issue. The requesting FMM is to tag them for review once the issue has been completed.

### Current asks into the Sales Development Org
* [AMER](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues?scope=all&state=opened&label_name[]=SDR%3A%3AAMER%20Event%20Awareness
)
* [US PubSec](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues?scope=all&state=opened&label_name[]=SDR%20Pub%20Sec)
* [APAC](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues?scope=all&state=opened&label_name[]=SDR%3A%3AAPAC%20Event%20Awareness)
* [EMEA](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues?scope=all&state=opened&label_name[]=SDR%3A%3AEMEA%20Event%20Awareness
)

#### Field Process

When our Field Marketing Managers (FMMs) begin preparing for an event or campaign, they will create an issue using a [regional issue template](https://gitlab.com/gitlab-com/marketing/field-marketing/tree/master/.gitlab/issue_templates). If they need BDR or SDR Support they will open an [SDR Request issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=ISR_SDR_FMTemplate) and the FMM will loop in the appropriate BDR/SDR manager(s) on this issue as well as fill out/ensure the following sections are accurate:
* Pre-event/campaign goals
* Post-event/campaign goals
* Targets (title and number of attendees)

Once the sections are populated and up to date, the BDR/SDR manager is looped in. They will then elect an BDR/SDR with bandwidth to begin completing the steps in each of the above sections. The BDR/SDR will:
* Read the issue to get an understanding of the event
* Complete the tasks under the ‘Pre-event/Post-event’ section based on the deadlines next to each bullet.
* Slack their manager and/or the FMM if they have questions.

#### Corporate Process
The corporate process is the same as the field process detailed above with the exception that they have a separate issue when looping in the Sales Development Organization for assistance that will link to the larger event issue.

#### Event Deadlines
**Event Promotion**

*  The BDR/SDR manager needs to be assigned a minimum of 45-days prior to the event, but the sooner they can start prepping, the better.
*  The BDR/SDR manager(s) should have their teams begin compiling lists as soon as possible. Focus lists need to be completed three weeks prior to the event so that we will have three weeks of promotion
*  The BDR/SDR project lead needs to have their focus account list as well as the shared sequence completed, reviewed, and approved at least three weeks prior to the event so that we will have three weeks of promotion
*  **For strategic marketing campaigns categorized as direct mail or workshops, where there was BDR assistance in strategically driving attendence, we will ask marketing to import campaign MQLs and other designated "Hot" leads to the BDR assigned on the associated account.**

**Event Follow Up**

*  SDR project lead needs to have the follow up sequence created and reviewed with management and FMM one week prior  the event. The copy doc for the event is linked in the event Epic and can be used for reference.
*  FMM will ensure leads from Field Marketing initiatives will be routed to the SDR within 48 hours.

**Sequence Information**
- Please clone one of the Master Event Sequences found in the [Events Outreach collection](https://app1a.outreach.io/sequences?content_category_id%5B0%5D=22) and pre-populate as many of the variables as possible. If all of the variables can be populated manually or through Outreach/Salesforce, change the email type from ‘manual’ to ‘auto.’ Work with your manager and the FMM to make any other additional adjustments that may be needed.

#### Outbound Campaign Process

[Process Overview](https://www.youtube.com/watch?v=b-5cQX3-aZ0)

BDRs have found particular success when organizing campaigns alongside regional Field Marketing Managers to ensure repeatable tactics and measurable results. The workflow below outlines what that process would entail should it be determined that you leverage this motion.

**Process Steps**

**Step 1:** Speak with your BDR Manager to identify the Field Marketing Manager (FMM) assigned to your SAL’s & AE's/Territory/Targeted Accounts. Set a meeting with the FMM to discuss existing Campaigns in motion, your ideas for Campaigns, and previously successful Campaigns (15-minute meeting over Zoom).
[Example of a Successful Campaign](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/1750)

**Step 2:** Once you've identified a Campaign to launch, work closely with your FMM (FMM will create an Issue in GitLab related to the campaign), and then go into Salesforce Reporting to generate a targeted Contact list for the campaign.

**Workflow for creating a Salesforce report** (Screenshots below):
Go to Reports -> Create New -> Make a Report under Contacts & Accounts -> Add Field Filter Logic (BDR Assigned equals BDR Name, Contact Status does not contain Unqualified & Title Contains: Software Development, DevOps, Engineering, Security, Operations) -> Custom Date Range = from: last two years up to: the last two weeks and type in Last Interesting Moment into the field section and drag/drop the field into the Column Preview.-> Tip: - Organize your fields on the report to make it easier to read (First Name, Last Name, Account Name, Title, LIM, State, Country, Phone, Mobile, Email, Account Owner -> Run Report.

Please note, you can use this same Report workflow for generating a Lead Report. Simply select Leads from the New Report Options. Before you run the Report, please make sure that the Report Options -> Show is showing All Contacts/All Leads not My Contacts/My Leads by clicking the drop down and selecting All Contacts/Leads.

Save your Report by clicking the Save As tab. Make sure to Save Report under Business Development Representatives folder and add a Unique Report Name (see example)

Add Campaign name from SFDC Campaign that is listed in the Field Marketing BDR specific Issue. Example: 20200909_TShirtSurveyOutreachCa -> Member Status = Member -> Click Add.

Your Report for your FMM Campaign is now complete! Keep your Unique Report Name in mind for when you upload Report to Outreach (see Step 4).

**Step 3:** It's time to generate content for your Campaign in Outreach. Note, if the Campaign previously ran, use existing content (FMM will confirm). Otherwise, work with your FMM and BDR/SDR Manager to delegate the resources you need to create content for the Campaign.
[Outreach Campaign Example](https://app1a.outreach.io/sequences/5957?accounts-nonce=5583950dda94071d35e1)

**Step 4:** Bulk add Contacts or Leads pulled from Step 2 to the Campaign in Outreach (ensure Campaign content is revised and reviewed by BDR Manager and FMM).
**Workflow for Bulk Creating an SFDC Report in Outreach for Sequencing:**
In your Outreach 360 view, in the top left corner select Actions -> Select Bulk Create -> Select SFDC Report and type in your Unique Report Name

Select Next, then re-map any missing fields. Once Report is Uploaded and you are in Prospect View, select the Prospects you want to Add to Sequence and then add to the Campaign Sequence. You have now uploaded your Target Contact or Lead List SFDC Report into Outreach and Sequenced List into Campaign for Outbound prospecting!

**Step 5:** After Contacts or Leads are loaded into the appropriate Campaign Sequence, alert your Manager and FMM in the appropriate issue for review before activating (FMM approval recommended to start Campaign in Outreach).

**Step 6:** Check the Campaign daily in Outreach throughout the duration to ensure all tasks are completed. Provide updates in the Issue created in GitLab by the FMM related to the campaign (updates can include IQM conversion rate, Open rates, IQM to SAO conversion rates, etc.)

**Step 7:** Upon commencement of the Campaign, post a final update in the Issue that includes your results (updates can include the number of IQMs, Open rates, IQM to SAO conversion rates, etc.)

**Step 8:** Recommended to schedule 15-minute meeting with FMMs to recap the Campaign and provide feedback (this meeting can focus on final results, and if the Campaign should stop, continue, or be revisited in the future).

Optional Post Event Follow Up: Continue to monitor Contacts and Leads uploaded into the Campaign for lingering results (there are sometimes stragglers on Campaigns that respond after the Campaign has concluded). An easy way to monitor is setting one or two tasks in Outreach for 2 weeks and 4 weeks after the Campaign has concluded.


## PubSec AMER BDRs
We currently have 4 BDR's focused on supporting the PubSec ISR's in the US. Their alignment is as follows:

| PubSec BDR | Group |
| ------ | ------ |
| MaKayla Emahiser| Army, Navy, Federal Systems Integrators (FSI) |
| Ricky Wooten | NSG 1-2, 4th Estate, Air Force |
| Melinda Ryan | Civ 1-7, NSG 3-6 |
| Matt Wyman | State, Local, Edu (SLED) |

The US PubSec team covers all US Agencies, US Government Contractors (Federal Systems Integrators), and US-based Universities.

A Federal Systems Integrator (FSI) is a private company with 75% or more of their business coming from the Government and/or whom requires US-based support. In the case that a commercial SDR/BDR comes across a company that may or may not meet this ROE because they do both Government and Commercial work, they should work with the FSI BDR to determine if it meets the ROE. If it is not possible to determine if it meets the criteria and the prospect is definitively discussing Government work, the Commercial SDR/BDR must air on the side of caution and let the FSI BDR handle the lead until the criteria can be determined.

The US PubSec team does not cover Universities, Agencies, and Contractors who are not based in the US. These prospects will be routed to the respective Commercial BDR/SDR covering that territory.

PubSec BDRs have a different workflow from other BDRs in that they do not convert leads into opportunities. They input qualification notes on the lead record after their intro call and then set up the IQM with the ISR. The ISR converts the lead into the opportunity to ensure the opp is assigned to the correct account. Once the ISR has determined our SAO criteria has been met, they are responsible for putting the BDR on the opportunity and moving the opportunity into discovery.

The PubSec BDRs will work closely with PubSec FMM's and MPM's on pre and post-event outreach. For events that are limited in attendance capacity, the PubSec ISRs will own pre and post-event strategy, not the BDR's.

[SDR PubSec Issue Board](//gitlab.com/groups/gitlab-com/-/boards/1864446?label_name[]=SDR%20Pub%20Sec) - used to track relevant GitLab issues involving the PubSec BDR team. This is a global issue board and will capture all issues in any group/sub-group in the GitLab.com repo when any of the following scoped labels are used.


- `SDR Pub Sec`- Issues concerning the PubSec BDR team. When PubSec FMM's/MPM's add this tag to an event, these issues will appear on our board. When applicable, the PubSec BDR manager will create a BDR follow-up issue and relate it to the event issue so that marketing members can easily track our work.
- `SDR Pub Sec:: FYI` - Needs attention.
- `SDR Pub Sec:: In Progress` - Related work needed is in progress.
- `SDR Pub Sec:: Completed` - Related work is completed and ready for BDR manager to review/close.
- `SDR Pub Sec:: Revisiting` - Campaign is being revisited and additional outreach is being done.
- `SDR Pub Sec:: Josh FYI` - Issues pertaining specifically to Josh.


## Global Enterprise Land BDR Team
The Enterprise Land BDR team is responsible for creating qualified meetings and sales accepted opportunities with net new customers in the Large - Enterprise Segment. The team is composed of BDRs who focus on account-centric, persona-based outreach, strategic outbound prospecting, social selling, and creative outreach to educate and create internal champions that align with the GitLab value drivers and use cases.

### AMER Enterprise Land Alignment

East Land
- **Manager:** Shamit Paul
- Eduardo Guillen
- Michael Lerner
- Micah Lockerby
- John Hegarty
- Maria Spada
- Kelsey Bartlett
- Jason Yanelli

West Land
- Alex Demack
- Paul Oakley
- James Gray

LATAM Enterprise
- Leo Vieira

These BDRs are aligned by accounts - [see Salesforce report](https://gitlab.my.salesforce.com/00O4M000004aftz).

If you are trying to route a lead and the account is First Order Available and does NOT have an BDR Assigned on the account, please chatter Shamit Paul and she will assign to the BDR via round robin routing.

### EMEA Enterprise Land Alignment

| **Role**         |  **BDR**             |  **Lead & Account Assignments**      |
| :------------- |  :--------------------------------- | :----------------- |
| Northern Europe - Land       |  Kalyan Nathadwarawala   |  UKI   |
| NE & MEA - Land       |  Wasan Al-Nahi   |  UKI(Warren), Benelux(Nasser), Middle East, Africa  |
| Nordics - Land      |  Sarah Van Damme  |  Nordics, Benelux(Aslihan)  |
| CEE - Land       |  Eltaj Muradli   |  Central, Eastern Europe  |
| Southern Europe - Land       |  Romain Santos   | France, Southern Europe  |
| DACH - Land       |  Christopher Allenfort   | DACH (Germany, Switzerland, Austria)  |

### Expectations

**Land BDR**

Named BDRs are responsible for:
* Leading account planning efforts and creating in-depth account plans for top focus accounts
* Qualifying and converting marketing-generated leads (MQLs) and inquiries into sales accepted opportunities (SAOs)
* Aligning prospecting efforts to the field, corporate, strategic and account based marketing campaigns
* Educating prospects and making positive impressions
* Generating awareness, meetings, and pipeline for sales
* Collaborating with peers, marketing, and sales teams

Requirements:
*  Meet monthly quota
*  Maintain a high sense of autonomy to focus on what's most important
*  Participate and lead planning, execution, and cross-functional meetings
*  Participate in initial qualifying meetings, discovery calls, and follow-up conversations
*  Maintain exceptional Salesforce hygiene, logging all prospecting activity, conversations, and account intelligence uncovered
*  Generate IACV Pipeline

### Working with Sales
BDRs and SDRs should be in regular communication with the SALs that they support. When taking on new accounts or supporting a new SAL, it is the responsibility of the  BDR/SDR to set up a kickoff call to:
1. Review the focus accounts
2. Gather actionable intelligence from SALs/AEs
3. Review outbound BDR plays, campaigns, field tactics, and other offers
4. Document the plan using docs, a spreadsheet, or the Sales Development quarterly issue plan

Recurring SAL<>BDR/SDR meetings can be scheduled or take place on an as-needed basis. As BDRs/SDRs are not directly aligned with SAL we encourage async updates as much as possible

Slack is encouraged for informal communication, but anything related to strategy, approach, or outreach should be documented.


### Geo BDR Account Prioritization Model

**Account Scoring & Prioritization**

The next step after identifying all of the Net New focus accounts in a region is to prioritize them. The scoring model below should be used to determine the Net New Account Tier which will help guide prioritization and prospecting efforts using the Outbound Prospecting Framework (below).

`A Tier Accounts` - Have at least 4 out of 5 qualifiers below

`B Tier Accounts` - Have 3 out of 5 qualifiers below

`C Tier Accounts` - Have 2 out of 5 qualifiers below

`D Tier Accounts` - Have 1 out of 5 qualifiers below

`F Tier Accounts` - Have 0 out of 5 qualifiers OR zero direct revenue potential in the next 3 years

**Account Scoring Qualifiers:**
*  Current CE Usage
*  250+ employees in IT/TEDD positions
*  Good Fit Industry / Vertical (High Growth, Technology, Financial, Healthcare, Regulated Business)
*  Early Adopters / Innovative IT Shops (Identifiers & Keywords): Kubernetes / Containers, Microservices, Multi-cloud, DevOps, DevSecOps, CICD (and open-source + proprietary tools), SAST / DAST, Digital Transformation
*  Current DevOps Adoption (multiple DevOps roles on staff or hiring for multiple DevOps positions)

**Outbound Prospecting Framework**

| **Tier**        |  **Goal**                 | **Priority Level**      | **Outbound Tactics**      |
| :---------- |  :----------                  | :----------------- | :----------------------|
| A Accounts   |    Conversations, IQMs, MQLs         |      High (60% of focus)      | Hyper-personalized, simultaneous outreach, creative, direct mail, ads, groundswell, events |
| B Accounts   |  Conversations, IQMs, MQLs           |      High (30% of focus)      | Hyper-personalized, simultaneous outreach, creative, direct mail, ads, groundswell |
| C & D Accounts |  Conversations, MQLs               | Low  (< 10% of focus)         | Automated role- / persona-based outreach, groundswell |
| F Accounts |  Eliminate from BDR focus account lists   | Low (< 2% of focus)     | Do not focus on or attempt to qualify |



## Global Enterprise Expand BDR Team
The Global Enterprise Expand BDR team is responsible for creating qualified meetings, sales accepted opportunities, and pipeline within existing customer accounts (Accounts with total CARR in parent/children/subsidiaries in a hierarchy) in the Large - Enterprise Segment.   
The team is composed of BDRs who focus on strategic account-based, persona-based outreach, strategic outbound prospecting, social selling, and creative outreach to create and expand internal champions that align with the [GitLab value drivers](https://about.gitlab.com/handbook/sales/command-of-the-message/#customer-value-drivers) and use cases.   
The aim for the BDR is to find new business units within these customer account hierarchies who are looking to move from a competitor/GitLab Core/CE to a paid version of GitLab.  
SALs alone are responsible for renewing/uptiering existing paid customer instances to a higher paid version of the product, eg from Standard to Premium.

### How GitLab defines a customer

Account alignment is based on the ultimate parent. If any account within the ultimate parent hierarchy is a customer and has a `Total CARR` amount of $48 or over, then the entire hierarchy is considered a customer and worked by the Expand team.  

Total CARR of $48 and below is deemed as a Land Account and these accounts are flagged with a check mark in the `First Order Available` field on the account object.

Accounts with no check in the box fall to the Expand team.


## Global Enterprise Expand Alignment

AMER Enterprise BDR Expand Team is aligned to account territories in US West and US East regions. Each region has verified Growth scores/CARR per territory that help determine the equitable spread of BDR alignments as shown below. Target accounts are based on SAL strategy, potential CARR, Intent data (propensity to buy), and industry.  

| **Role**                    | **BDR**                | **Territory Alignment**                                                                                                                                                                                             | **Strategic Account Leader**                                                   |
|-------------------------|--------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------|
| AMER   | Andrew Finegan     | Large-AMER-Central-3, Large-AMER-Central-2, Large-AMER-Northeast-4, Large-AMER-Southeast-4                              | Tony Scafidi, Brandon Greenwell, Matt Pretrovick, TBH       |
| AMER   | Bryan Tibbitts     | Large-AMER-Southeast-1, Large-AMER-Southeast-2, Large-AMER-Central-4, Large-AMER-Northeast-5,                               | Patrick Byrne, Chris Graham, Ruben Govender, Katie Evans  |
| AMER   | Erica Wilson       | Large-AMER-Southeast-3, Large-AMER-Northeast-2, Large-AMER-Northeast-1, Large-AMER-East-Named-4                                | Paul Duffy, Jim Bernstein, Josh Rector, Jon Schuster     |
| AMER   | Max Chadliev       | Large-AMER-Northeast-3, Large-AMER-Central-1, Large-AMER-East-Named-2, Large-AMER-East-Named-5, Large-AMER-East-Named-6,                                 | Mark Scheuber, Peter McCracken, Sean Billow, Tim Kuper  |
| AMER   | Steven Cull        | Large-AMER-East-Named-1, Large-AMER-East-Named-3, Large-AMER-Southeast-5,                                | Mark Bell, Larry Biegel, April Marks, Chip Digirolamo      |
| AMER   | Léa Tuizat         | Large-AMER-West-1, Large AMER-PNW/MW-1, Large-AMER-Bay Area-4, Large-AMER-PNW/MW-3, Large-AMER-Rockies/SoCal-1, Large-AMER-Rockies/SoCal-2, Large-AMER-Rockies/SoCal-3,                                 | Adi Wolff, Brad Downey, Joe Miklos, Ashley Meiss, Todd Payes |
| AMER   | John Belardi       | Large-AMER-Bay Area-3, Large-AMER-Bay Area-5, Large-AMER-PNW/MW-2, Large-AMER-PNW/MW-4, Large-AMER-Rockies/SoCal-4                               | Michael Nevolo, Philip Wieczorek, Joe Drumtra, Steve Clark, Nico Ochoa   |
| AMER   | Tyler Thompson     | Large-AMER-West-2, Large-AMER-Bay Area-1, Large-AMER-Bay Area-2, Large-AMER-Bay Area-6, Large-AMER-PNW/MW-5, Large-AMER-Rockies/SoCal-5                               | Jim McMahon, Michael Scott, Alyssa Belardi, Lydia Pollitt, Robert Hyry   |

### EMEA Enterprise Expand Alignment

EMEA Enterprise BDR Expand Team is aligned to account territories in Northern Europe, DACH, France/Southern Europe, and emerging markets. Target accounts are based on SAL strategy, potential CARR, Intent data (propensity to buy), and industry.

| **Role**         |  **BDR**             |  **Lead & Account Assignments**      |
| :------------- |  :--------------------------------- | :----------------- |
| SE - France       |  Bastien Escude   |  Hugh Christey, Théo Deschamps-Peugeot, Yannick Muller, Simon Duchene   |
| DACH       |  Julian Bede   |  Christoph Stahl, Dirk Dornseiff, Christoph Caspar  |
| DACH / CEE     |  Lukas Mertensmeyer  |  Dennis Pfändler, Rene Hoferichter, Timo Schuit, Vadim Rusin  |
| UKI / UKI PubSec - SE (Spain,Italy)       |  Randeep Nag   |  Peter Davies, Jessica Cousins, Phillip Smith, Pierre Goyeneix, Hugo Alvarez Rodriguez  |
| Benelux / Nordics       |  Tatiana Fernandez   | Annette Kristensen, Aslihan Kurt, Nasser Mohunlol  |
| UKI       |  Tom Elliott   | Godwill NDulor, Warren Searle, Justin Haley, Nicholas Lomas, Steven Challis  |

## EMEA Mid-Market BDR Alignment

The EMEA MM FO team is split equally between North and South Europe. The 100-500 Segment is primarily targetting UKI, France and Germany as a P0 territory and the P1 territories below as a secondary target.

The team's baseline outbound activity revolves around at least 20 accounts per SDR per week. 15 from the P0 territories and 5 from their P1 territories. First Order BDRs source all 20 accounts from their territories.

|**Segment**|**BDR**|**Territory**|
| ------ | ------ | ------ |
| 500 - 1500 | Johan Rosendahl | P0 - Nordics, Netherlands, Germany, Austria, Spain  |
| 500 - 1500 | Dipeeka Kank | P0 - UKI, France, Italy, Switzerland |
| 100 - 500 | Luis Calixto, Barbara Schreuder, Raquel Gulin, Denis Stete | P0 - UKI, France, Germany |
| 100 - 500 | Luis Calixto | P1 - Nordics |
| 100 - 500 | Barbara Schreuder | P1 - Austria, Switzerland, BeNeLux |
| 100 - 500 | Raquel Gulin | P1 - South Europe |
| 100 - 500 | Denis Stete  | P1 - East Europe   |  

In case of any AE requests for outbounding, the go-to BDRs for each Sales territory are below and [may also be found here](https://docs.google.com/spreadsheets/d/1Zx9SEcPmOtSFPjJEQgoRsO3n4GFAJehxvgbS1-onqZg/edit#gid=2102471434). The BDR team is briefed to incorporate as a priority any request for OB collaboration from AEs.

|**Segment**|**BDR**|**Territory**|
| ------ | ------ | ------ |
| 100 - 500 | Luis Calixto | Nordics, UKI |
| 100 - 500 | Barbara Schreuder | Austria, Switzerland, BeneLux, MEA |
| 100 - 500 | Raquel Gulin | South Europe, France |
| 100 - 500 | Denis Stete  | East Europe, Russia, Germany   |

## Expectations

### Expand BDRs are responsible for:

- Accounts with their name in the `BDR Assigned` field
- Leading account planning efforts with SALs/TAMs/ISRs and creating in-depth account plans for top focus expand accounts and other accounts in their hierarchy
- Qualifying and converting marketing-generated leads (MQLs) and inquiries into sales accepted opportunities (SAOs)
- Aligning their outbound/inbound prospecting efforts to field/corporate/strategic/account based marketing campaigns
- Educating prospects and making positive impressions
- Generating/Expanding GitLab awareness, meetings, and ARR pipeline for sales
- Collaborating with peers, marketing, and sales teams


### Requirements:

- Maintain a high sense of autonomy to focus on what's most important
- Participate in lead planning, execution, and cross-functional meetings
- Participate in initial qualifying meetings, discovery calls, and follow-up conversations
- Maintain exceptional Salesforce hygiene, logging all prospecting activity, conversations and account intelligence uncovered, including LinkedIn conversations and calls as well as emails.


## Working with Sales
Expand BDRs should prioritize communication with the SALs within territories they are aligned to. BDRs will organize and schedule a kickoff call to:
- Review the focus expand accounts (including all children/subsidiaries of over 500 employees)
- Gather actionable intelligence from SALs/TAMs/ISRs and discuss key expand strategy
- Research the account with the SALs/TAMs/ISRs, see if LinkedIn introductions can be made
- Research the account on LISN and select 10 - 15 contacts who follow GitLab or who fall into the personas we target
- Review outbound BDR plays, campaigns, field tactics, and other offers
- Review [Outreach persona sequences](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#outbound-messaging) with SAL:  
- Document the plan using docs, a spreadsheet, and/or the BDR quarterly issue plan

Recurring SAL|BDR meetings can be scheduled or take place on an as-needed basis.

Slack is encouraged for informal communication (team slack channel), however, anything related to strategy, approach, or outreach should be documented.

## Expansion Strategies, Account Ranking, and Rules of Engagement

### Strategies:
- Growth within an existing customer account: BDR should strategically partner with SAL/ISR to identify additional business units to bring into the conversation as growth opportunities. Renewal dates should be exploited.
- Expansion into parent/children/subsidiaries of existing customer accounts: These are accounts that fall within the corporate hierarchy within Salesforce. If a child/subsidiary account is identified via ZoomInfo but not in our system, then BDR should follow the following process:
Create a LEAD for that account and then convert to CONTACT to create the ACCOUNT when you qualify your first lead.
- Free to paid upgrades: Existing Core/CE users can be targeted to upgrade to a paid version of GitLab

### [Ranking](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#definitions-for-enterprise-account-ranking) and prioritization of accounts:
- Rank 1: Expand with Purpose (EwP) accounts should be closely/strategically worked with SALs. These accounts have High Demandbase intent scores and should therefore be High touch by BDR.
- Rank 1.5: As above but nominated for ABM and should also be worked by BDRs
- Rank 2: ICP Ultimate parent accounts that have Core/CE-Users in a hierarchy, Total CARR/LAM for account, Med/Low Demandbase intent scores, and small renewals in current FQ.
- Rank 3: As above but are not our ICP

### Professional Services Opportunities

A **Professional Services Opportunity** will be used to cover any integration, consulting, training or other service that a Sales rep will sell to a prospect/client and needs or wants to be invoiced separately. To invoice separately a new quote and opportunity must be created.

More information on the steps needed to create a Professional Services Opp can be [found here](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#creating-a-professional-services-opportunity)

## Leveraging Business Development Associate (BDA) Team

Having the Business Development Associate Team gives us the ability to develop individuals with loads of alignment to our GitLab values and limited Sales Development experience into strong SDR ready candidates. We do this by exposing them to different projects that strengthen their familiarity with our [tools](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/sales-development-tools/), processes, systems, messaging, teams, and GitLab culture. These projects ultimately are also aimed to support the Sales Development Organization in reaching their goals so that it's an all around win-win. Projects to date include:
- Helping route MQLs that are in admin-related ownership
- Sequencing lower level leads that are older but weren't touched because they were in admin related ownership
- Helping set up next step tasks for leads in qualifying status
- Sourcing net new leads from a specific set of accounts and then sequencing on behalf of the BDRs and SDRs
- Identifying leads related to targeted accounts that haven't been sequenced and yet are still currently at the organization
- Reaching out to Drift leads that did not get to have a conversation with someone
- Participating in new tool initial roll-out group

### Sourcing Process
The manager will create a list of their focus area and criteria similar to what's outlined [here](https://docs.google.com/document/d/1oOzjd2j8trQLU_5h_oiBbyg-saRMe-TA8HTDBuiPuEM/edit?usp=sharing).
Past Business Development Associates have shared their best practice steps related to the sourcing process [here](https://docs.google.com/document/d/1dzRFJlSDdU9Y39d5j6pkFl4i_lyEs9EONQSF14ZcnSM/edit?usp=sharing).

## Sales Development Organization Time Off

While we have a "no ask, must tell" PTO policy, we ask that in accordance with the [Time Off Policy](https://about.gitlab.com/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off) in the handbook that you give your manager a heads up early of your intended time off so that they can help arrange cover for your tasks, if necessary. If you are taking off more than one day, please give your manager at least a week's notice.

## Issue Boards & Team Labels

[Sales Development Org Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/707128): used to track relevant GitLab issues involving the Sales Development Org. This is a global issue board and will capture all issues in any group/sub-group in the GitLab.com repo when any of the following *scoped* labels are used.

[Sales Development Org Event Tracker](https://gitlab.com/groups/gitlab-com/-/boards/1718115): used to track upcoming events globally.

- `SDR` - issues concerning the Sales Development Organization. This label will typically be removed and changed to one of the below labels once accepted by our team.
- `SDR::Priority` - projects that we would like brought into RevOps meeting for feedback/next steps from other teams
- `SDR::Planning` - Discussion about next steps is in progress for issues concerning the Sales Development Organization
- `SDR::In Progress` - BDR/SDR action item is presently being worked on
- `SDR::On Hold` - Project is put on hold after agreement from BDR/SDR leadership team
- `SDR::Watching` - No direct SDR/BDR action item at this time, but BDR/SDR awareness is needed for potential support/questions
- `SDR::Enablement Series` - Label to track and monitor upcoming topics for the Sales Development Org enablement series. All of these issues roll up to this epic.
- `SDR::AMER Event Awareness` - Americas Sales Dev Org awareness is needed for potential support/questions in regard to events
- `SDR::APAC Event Awareness` - APAC Sales Dev Org awareness is needed for potential support/questions in regard to events
- `SDR::EMEA Event Awareness` - EMEA Sales Dev Org awareness is needed for potential support/questions in regard to events
- `SDR Pub Sec` - PubSec Sales Dev Org awareness is needed for potential support/questions in regard to events
- `SDR West Staff Request` - Utilized when a West BDR needs to be assigned to an issue

## Making Changes to the GitLab Handbook (for BDRs and SDRs)

[Video Walkthrough of how to make changes to the GitLab Handbook for the Sales Development org](https://www.youtube.com/watch?v=P7Nv7bzksiY&t=1032s)

One of our Values is being [handbook first](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/). In order to align the SDR organization more closely to this ideal, below are suggested steps. Please remember that the Handbook is a living document, and you are strongly encouraged to make improvements and add changes. This is ESPECIALLY true when it comes to net new solutions that should be shared so the whole organization has access to that process. (aka The DevOps ideal of turning "Localized Discoveries" into "Global Knowledge".)

Steps:

1. Have a change you want to make to the handbook? Great!
2. Navigate to the source code of the page in the handbook (e.g. [Link to edit the SDR page in the Handbook](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/handbook/marketing/revenue-marketing/sdr/index.html.md) )
3. Click either "Edit" or "Web IDE" to make your changes.
4. Have a brief but descriptive "Commit message" (e.g. "Add new section on 'How to Make Changes to the Handbook'") and commit your changes
5. Fill out the Merge Request details


## Working with 3rd Parties (resellers, partners, consulting firms, and/or companies that buy GitLab for their customers)
**The end-user account determines the BDR/SDR alignment:** If you are the SDR assigned to the 3rd party lead, please gather the information in point 1 and pass the lead to the correctly assigned BDR (the BDR assigned to the end-user account) who will complete points 2-6

1.  Gather billing and end-user details from the reseller:
    * Billing company name/address:
    * Billing company contact/email address:
    * End-user company name/address:
    * End-user contact/email address:
    * [Snippet in outreach](https://app1a.outreach.io/snippets)
2. Create a new lead record with end-user details
    *  Ensure that all notes are copied over to the new LEAD as this is the LEAD that will be converted.
3. Converting the new lead
    * Name opp to reflect reseller involvement as shown here: “End-user account name via reseller account name”
4. Convert original reseller lead to a contact associated with the reseller account
    *  If an account does not already exist for the reseller, create one when converting the lead to a contact.
    *  Assign the record to the same account owner
    *  Do NOT create a new opportunity with this lead.
5. Attach activity to the opportunity
    * On the reseller contact, go to the activity and link each activity related to your opportunity to the opp.
        * Activity History > click edit to the left of the activity > choose 'opportunity' from the 'related to' dropdown > find the new opportunity > save
6. Update the opportunity
    * Change the business type to new business and stage to pending acceptance.
    * Under contacts, add the reseller contact, role as reseller, and primary contact.
    * Under partners, add the reseller account as VAR/Reseller"

## Working with the Community Relations Team
The [Community Relations team](/handbook/marketing/community-relations/) owns GitLab's [Education](/handbook/marketing/community-relations/education-program/), [Open Source](/handbook/marketing/community-relations/opensource-program/), and [Startups](/solutions/startups/) programs. When a lead fills out the form to apply for one of these free community programs, Salesforce `Lead` ownership will automatically change to the `Community Advocate Queue`. If this Lead was in an Outreach sequence, it will automatically be marked as finished.

The [Community Operations team](/handbook/marketing/community-relations/community-operations/) (a sub-team of Community Relations), and the Program Managers for each program, will then work to qualify the lead. If the Lead does not end up qualifying for one of the programs, they will be passed straight to sales.
- Forms
     -   [GitLab for education](https://about.gitlab.com/solutions/education/)
     -   [GitLab for Startups](https://about.gitlab.com/solutions/startups/)
     -   [GitLab for Open Source](https://about.gitlab.com/solutions/open-source/)
- Questions
     - Chatter the relevant Program Manager in Salesforce. For EDU, OSS, and Startups `@Christina Hupy`.
     - Slack channel: `#community-programs`
