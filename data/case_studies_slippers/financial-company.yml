title: Financial Company
cover_image: /images/blogimages/financial_company_case_study_image.png
cover_title: |
  GitLab CI keeps financial institution secure and scalable
cover_description: |
  GitLab replaces Jenkins and multiple Git-based tools to achieve open source stability and security for prominent financial organization
canonical_path: /customers/financial-company/
twitter_image: /images/blogimages/financial_company_case_study_image.png
customer_logo: /images/case_study_logos/financial-institution-logo.svg
customer_logo_css_class: brand-logo-tall
customer_industry: Financial Services
customer_location: North America
customer_solution: Premium
customer_overview: |
   Looking to become open source and cloud-based, a financial institution turned to GitLab for a secure transformation.
customer_challenge: |
  Using too many tools caused more work for the development team and created unnecessary costs in the financial company’s quest of moving to the cloud.
key_benefits: >-

    
    More cost efficient than competitor’s tools

   
    Helped the institution become open source

   
    The ability to deploy all the way to production

   
    Keeps critical data secure

customer_stats:
  - stat: |
      20,000
    label: running repositories
  - stat: |
        $250,000
    label: cost savings per quarter
customer_study_content:
  - title: the customer
    subtitle: A leader in financial planning
    content:  >-
    
   
       This American institution has offered financial services spanning two centuries.
       With millions of clients, the organization provides banking, insurance and wealth
       management services and products. It is ranked as one of the top investment firms
       in the United States.
    

  - title: the challenge
    subtitle: Too many tools, too many issues
    content: >-
    
   
       The financial company was looking to move toward cloud computing and embrace open source.
       While the development teams worked on various tools, it became apparent that using multiple tools
       was too complicated to accomplish their open source goals. “Within our organization, if you have a
       team who wants to see some other team's code, they have to go through a different set of processes,” said Ravi,
       engineering manager at the financial organization. “You have to get permissions in order to even see the code and
       there's no true collaborative experience. That was one big problem.”
    
   
       Moving back and forth between tools was an issue in both being able to communicate and also a hindrance in the daily
       grind. Not all the tools that were being used by the development team were automated, which meant that integration took
       extra steps. “Other git-derived tools, whether open source or not, are a lot of manual work. Which as the scale grows,
       work keeps growing alongside,” said Michael, software engineer. “It was becoming a burden, especially for me living in
       a DevOps world, to keep doing manual stuff and not evaluate all the tools that might make a lot of those steps more
       automated and easier to manage.”
    
   
       Other CI tools that the team had been working with used a disorganized array of plugins. “The real challenge we had with
       Jenkins was that every team was spinning up their own version of Jenkins. We had like 40, 50 different instances of Jenkins
       running all over the place with different versions, different kinds of plugins,” Ravi said. “Nobody was keeping track of what
       plugins are running on what and what software was running where.”
    
   
       Cost was another factor in the development team’s decision to pare down their tooling. As the company experienced, it can be
       expensive to keep a variety of tool services running.


  - title: the solution
    subtitle: Built-in capabilities lighten the developers’ workload
    content: >-
    
   
        GitLab was one of the tools that was originally being used by the developers. As GitLab usage expanded, the corporate
        team started taking notice. “GitLab started as an individual initiative first that was deployed on its own and kind of
        grew in an organic way. It wasn't a mandate,” Michael said.
    
   
        Developers were naturally drawn to GitLab’s capabilities. As they discovered challenges with other tools, it became clear
        that GitLab CI is simplified and has the ability to integrate easily with existing tooling. “The CI with Docker, the Docker
        support in the CI was really important because it allows the application teams to really self-manage their own builds' dependencies,”
        Michael said. “It's true that with GitLab CI, because we use the templates, there is a lot less need to add those third-party plugins
        to the CI system, for sure.”
    
   
        With GitLab CI, developers can deploy all the way to production. Pipelines are in one place and users can look at code at every step,
        which was a big deal for the financial institution. “It wasn't that way in the past. With other git-derived tools and Jenkins, you have
        to create integrations and you have to see the pipelines are different. It was challenging to write Jenkins files, so we faced a lot of
        challenges. So that kind of helped us with the GitLab CI,” Ravi said.
    
   
        GitLab covers a variety of tool offerings efficiently and the development team was able to create functions for other parts of the company.
        “As GitLab reported new capabilities, we were able to learn about them and then choose which one we would pick and choose to put into action
        for the rest of the teams,” Michael said. 
    
   
        As a financial institution, protecting client information is a top-priority. GitLab provides a level of security that is incomparable when
        using an assortment of other tools.



  - title: the results
    subtitle: Improved cycle time, scalability, and security
    content:
    
   
        With GitLab fully in place, the development team is focused on scaling upward. “We do a monthly release cycle, we don't do a lot of maintenance within GitLab,”
        Ravi said. “Our thought is to be more focused on how we scale this software, how we make it highly available, and other data warehousing technology. We're focusing a lot on that.”
    
   
        The development team is able to focus less on maintenance, generating an upswing in cycle time by using GitLab’s built-in templates. “Going from commit
        to production is actually really fast. So without too much work from the development team, you have all of the software ready for you,” Ravi said. “Once
        your environments are defined, you can create even the production deployment. So cycle time has improved a lot.”
    
   
        Thousands of repositories are migrating to GitLab on top of the over tens of thousands of repositories that are already in it. “We have grown because of
        the capabilities; there are a lot of teams that we're supporting now. Now that we have become the enterprise software, enterprise SCM, it's the entire
        company of thousands of developers who are creating software now. And so the number of Gitlab users has grown,” Ravi said.
    
   
        Because of GitLab’s transparency, the development team can know what to expect in order to plan for the future. Delivery rate of new features,
        sprint times, and even critical bugs are made public, so the financial company is aware of how best to move forward. “Being much more transparent
        about the general direction was a much better experience as a customer,” Michael said.


customer_study_quotes:
  - blockquote: We had these two types of software and the executive team was like, Why are we paying for both? Right? Other tools with the similar functionalities are very expensive, like 250, 300 dollars per user. GitLab offers a more economical option— especially since we can replace multiple tools.  And that combined, you take 400, and we had like 800 users duplicated across, and we were bringing a lot of that money back for the business.
    attribution: Ravi
    attribution_title: engineering manager
  - blockquote: We were becoming open source, more trending towards open source across the company. And GitLab being an open source software made sense for that for us.
    attribution: Ravi
    attribution_title: engineering manager 
  - blockquote: GitLab has been great for us. It is a good kind of a software that we love.
    attribution: Ravi
    attribution_title: engineering manager
