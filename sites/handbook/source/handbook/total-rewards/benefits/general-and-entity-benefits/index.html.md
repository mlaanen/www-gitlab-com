---
layout: handbook-page-toc
title: General & Entity Specific Benefits
description: A list of the General & Entity Specific Benefits that GitLab offers.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

For the avoidance of doubt, the benefits listed below in the General Benefits section are available to contractors and team members, unless otherwise stated. Other benefits are listed by countries that GitLab has established an entity or co-employer and therefore are applicable to team members in those countries only via our entity specific benefits. GitLab has also made provisions for Parental Leave which may apply to team members but this may vary depending on local country laws. If you are unsure please reach out to the Absence Management team.

## Contact Total Rewards
For any questions relating to benefits, please reach out to [Total Rewards](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group).

## Entity Benefits
- [GitLab BV (Netherlands)](/handbook/total-rewards/benefits/general-and-entity-benefits/bv-benefits-netherlands)
- [GitLab BV (Belgium)](/handbook/total-rewards/benefits/general-and-entity-benefits/bv-benefits-belgium)
- [Global Upside (India, Philippines, Kenya, Chile, Costa Rica, Denmark and Latvia)](/handbook/total-rewards/benefits/general-and-entity-benefits/global-upside-benefits-india)
- [GitLab Inc (US)](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us)
- [GitLab LTD (UK)](/handbook/total-rewards/benefits/general-and-entity-benefits/ltd-benefits-uk)
- [GitLab GmbH (Germany)](/handbook/total-rewards/benefits/general-and-entity-benefits/gmbh-benefits-germany)
- [GitLab PTY (Australia & New Zealand)](/handbook/total-rewards/benefits/general-and-entity-benefits/pty-benefits-australia)
- [GitLab Canada Corp](/handbook/total-rewards/benefits/general-and-entity-benefits/canada-corp-benefits)
- [GitLab GK (Japan)](/handbook/total-rewards/benefits/general-and-entity-benefits/gitlab-gk)
- [GitLab Ireland LTD](/handbook/total-rewards/benefits/general-and-entity-benefits/gitlab-ireland-ltd)
- [GitLab Korea LTD](/handbook/total-rewards/benefits/general-and-entity-benefits/korea-ltd-benefits)
- [Gitlab France S.A.S.](/handbook/total-rewards/benefits/general-and-entity-benefits/france-sas)
- [GitLab Singapore Pte Ltd](/handbook/total-rewards/benefits/general-and-entity-benefits/singapore-pte-ltd)
- [Remote Technology (Denmark, Spain, Italy, Switzerland, South Africa, Mexico, Hungary and Austria)](/handbook/total-rewards/benefits/general-and-entity-benefits/remote-com)
- [Safeguard (Brazil)](/handbook/total-rewards/benefits/general-and-entity-benefits/safeguard/)

### Benefits Available to Contractors

Contractors of GitLab BV are eligible for the [general benefits](/handbook/total-rewards/benefits/#general-benefits), but are not eligible for entity specific benefits. A contractor may bear the costs of their own health insurance, social security taxes, payroll administration, or tax details. 

Our contractor agreements and employment contracts are all on the [Contracts](/handbook/contracts/) page.


## Spending Company Money

GitLab will [pay for the items you need to get your job done](/handbook/spending-company-money).

## Equity Compensation

[Equity Compensation](/handbook/stock-options/) is offered to most GitLab team members. We strongly believe in team member ownership in our Company. We are in business to create value for our shareholders and we want our team members to benefit from that shared success.

## Life Insurance

In the unfortunate event that a GitLab team member passes away, GitLab will provide a [$20,000](/handbook/total-rewards/compensation/#exchange-rates) lump sum to anyone of their choosing. This can be a spouse, partner, family member, friend, or charity.
* This benefit is not applicable if there is a separate life insurance policy as part of the [Entity Specific Benefits](/handbook/total-rewards/benefits/general-and-entity-benefits/#entity-benefits) package.
* For all other GitLab team members, the following conditions apply:
  * The team member must be either an employee or direct contractor.
  * The team member must have indicated in writing to whom the money should be transferred. To do this you must complete the [Expression of wishes](https://docs.google.com/document/d/19p4IN-msvs4H10teBessfPx-1wu8J7Vs67Es8iVLlL8/edit?usp=sharing) form. To do this, first copy the template to your Google Drive (File -> Make a copy), enter your information, sign electronically. To sign the document, use a free document signing program like [smallpdf](https://smallpdf.com/sign-pdf); or you can print it, sign and digitize. Sign, save as a pdf and upload to your Employee Uploads folder in BambooHR.
  * For part-time GitLab team members, the lump sum is calculated pro-rata, so for example for a team member that works for GitLab 50% of the time, the lump sum would be [$10,000](/handbook/total-rewards/compensation/#exchange-rates).

## Financial Wellness 

GitLab offers resources related to [Financial Wellness](/handbook/total-rewards/benefits/general-and-entity-benefits/financial-wellness/) to support team members in their financial journey. 

## Paid Time Off

GitLab has a "no ask, must tell" [time off policy](/handbook/paid-time-off) per 25 consecutive calendar days off.

## Growth and Development Benefit

GitLab supports team members who wish to continue their education and growth within their professional career. GitLab team-members are eligible for a reimbursement of up to $10,000 USD per calendar year (January 1st - December 31st) depending on tenure, performance, company need for the learned skill, and available budget. A course/program is considered to be included in the calendar year in which the course/program is paid/reimbursed (which should also be the same calendar year in which it ends). If a course/program ends in December, team members have a grace period until the end of January to claim for Growth & Development reimbursements from the previous calendar year. 

Budget estimations for the Growth & Development benefit are based on utilization rates from past years, adjusted for the size of the company. The budget is managed on a company-wide scale, not division-specific. For example, approving reimbursement for a team member in Engineering does not "take away" budget from other engineers. Eligibility is managed on an individual basis. Eligibility for this benefit is part of each GitLab team member's Total Rewards package.

The [Growth and Development page](/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development) covers information on the application process and the different types of expenses you can get reimbursed under this benefit.
 
## GitLab Contribute
[GitLab Contribute](/events/gitlab-contribute/) is our annual GitLab team event where we get together to interact with one another and cultivate our community. Since our team is scattered all over the globe, we try to plan a different location each year. Contribute is an optional event, so team members should not feel pressured to attend. This year, Contribute will be hybrid-first, enabling team members to watch and participate from anywhere.

From our first Contribute in Serbia to our virtual event in 2020, team members from around the world have built friendships and experienced the values in person. 
For more information and compilations of our past events check out our [previous Contributes (formerly called GitLab Summit)](/company/culture/contribute/previous/).

## Business Travel Accident Policy

[This policy](https://drive.google.com/a/gitlab.com/file/d/0B4eFM43gu7VPVl9rYW4tXzIyeUlMR0hidWIzNk1sZjJyLUhB/view?usp=sharing) provides coverage for team members who travel domestic and internationally for business purposes. This policy will provide Emergency Medical and Life Insurance coverage should an emergency happen while you are traveling. In accompaniment, there is coverage for security evacuations, as well a travel assistance line which helps with pre-trip planning and finding contracted facilities worldwide.
   * Coverage:
      - Accidental Death [enhanced coverage]: 5 times Annual Salary up to USD 500,000.
      - Out of Country Emergency Medical: Coverage up to $250,000 per occurrence. If there is an injury or sickness while outside of his or her own country that requires treatment by a physician.
      - Security Evacuation with Natural Disaster: If an occurrence takes place outside of his or her home country and Security Evacuation is required, you will be transported to the nearest place of safety.
      - Personal Deviation: Coverage above is extended if personal travel is added on to a business trip. Coverage will be provided for 25% of length of the business trip.
      - Trip Duration: Coverage provided for trips less than 180 days.
      - Baggage & Personal Effects Benefit: $500 lost bag coverage up to 5 bags.
   * For any assistance with claims, please reference the [claims guide (internal only)](https://drive.google.com/file/d/1vmLjhebsf81N8oSxqlCihYg5q1WT8Efw/view?usp=sharing).
   * This policy will not work in conjunction with another personal accident policy as the Business Travel Accident Policy will be viewed as primary and will pay first.
   * For more detailed information on this benefit, please reference the [policy document](https://drive.google.com/file/d/1ktx_mhlEYyQoLrQJ7DhIcibQhrlnB-lb/view?usp=sharing).
   * If you need a confirmation of coverage letter, please reference the [visa letter generation document (internal only)](https://drive.google.com/file/d/1oesZnp-fVWWCakVejB7nTV39lntMFnSd/view?usp=sharing).
   * For any additional questions, please contact the Total Rewards Analyst.

## Immigration

GitLab offers benefits in relation to [obtaining visas and work permits](/handbook/people-group/visas/) for eligible team members.

## Employee Assistance Program

GitLab offers an Employee Assistance Program to all team members via [Modern Health](/handbook/total-rewards/benefits/modern-health).

## Incentives

The following incentives are available for GitLab team members:
   - [Discretionary Bonuses](/handbook/incentives/#discretionary-bonuses)
   - [Referral Bonuses](/handbook/incentives/#referral-bonuses)
   - [Visiting Grant](/handbook/incentives/#visiting-grant)

## All-Remote

GitLab is an [all-remote](/blog/2018/10/18/the-case-for-all-remote-companies/) company; you are welcome to [read our stories](/company/culture/all-remote/stories/) about how working remotely has changed our lives for the better.

You can find more details on the [All Remote](/company/culture/all-remote/) page of our handbook.

_If you are already a GitLab employee and would like to share your story, simply add a `remote_story:` element to your entry in `team.yml` and it will appear
on that page._


### Part-time contracts

As part of our Diversity, Inclusion & Belonging  value, we support [Family and friends first](/handbook/values/#family-and-friends-first-work-second) approach. This is one of the many reasons we offer part-time contracts in some teams.

We are growing fast and unfortunately, not all teams are able to hire part-time team members yet. There are certain positions where we can only hire full-time team members.

Candidates can ask for part-time contract during the interview process. Even when a team they are interviewing for can't accept part-time team members, there might be other teams looking for the same expertise that might do so. If you are a current team member and would like to switch to a part-time contract, talk to your manager first.

## Meal Train

In order to foster a sense of community and support fellow GitLab team members, if a team member is in need or experiencing a life disruption, they have the option of having a Meal Train started for them. A Meal Train is when a community comes together to support someone experiencing a difficult or disrupting event in their life who would benefit from having a meal provided to them during this time. 

Examples of when someone may request a meal train (this list isn't exhaustive):
* Arrival of a new baby
* Injuries/Surgeries
* Military deployment
* Extended illnesses
* Condolences

### Instructions for starting a Meal Train

1. You may create a Meal Train for yourself or for a fellow team member. If you are not the recipient of the Meal Train, please receive approval from the team member first and forward this to the Total Rewards team. We highly encourage managers to reach out to their team member to see if they would like a Meal Train if the manager informs them of a life disruption.
1. When creating a Meal Train, please create this in GitLab using the [Meal Train issue template](https://gitlab.com/gitlab-com/people-group/total-rewards/-/blob/master/.gitlab/issue_templates/meal_train.md).
    * Steps for completing the form are included in the issue template.
    * If you are completing this for someone else, please reach out to them to ensure the form is being completed accurately.
    * The Total Rewards team is also happy to create the issue on your behalf if you send the form details to total-rewards@ domain. 
1. The issue template has 5 pre-populated entries to add a date to, but the Meal Train can be longer or shorter than this. Please add as many dates that are needed and if at the end, you feel more are needed, please add the additional dates and notify the Total Rewards Analyst assigned to the issue that you have added dates so they can help gather support.

### Instructions for participating in a Meal Train

1. All Meal Trains are posted by a Total Rewards Analyst in the #whats-happening-at-gitlab and #total-rewards slack channels. You can also check the [Total Rewards issue tracker](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues) where all Meal Trains will be designated as such. 
1. If there is a Meal Train occuring, please read through the entire issue. If you are interested in participating, please edit the issue and add your name to the date you would like to contribute to. There is also a field to add the meal you will be providing or the meal delivery service you intend to gift. More instructions for how to proceed as a team member will be located in the issue.
    * Please note, it is typically only possible to gift a gift card from within the same country. The Total Rewards team is researching possible ways to participate from a different country than the recipient, but until we have a solution, we don't advise participating in this from a different country and can not help facilitate this.
1. Ensure you can follow through on the date you sign up for or please help coordinate a replacement as soon as you know you can no longer participate. 
1. Participation is voluntary and is **NOT** reimbursable by GitLab. Any expense incurred would be your responsibility. 

If you have any questions, please reach out to parental-leave@gitlab.com.

## Fertility & Family Planning

Team Members can refer to their [entity-specific](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#entity-benefits) medical plans to understand the fertility and family planning benefits available to them, as applicable. Please contact [total-rewards@gitlab.com](mailto:Total-Rewards@gitlab.com) should you have any questions.

GitLab plans to continue to iterate on fertility benefits as we evaluate and enhance our overall benefit plans. In a future state, GitLab aims to expand fertility and family planning services (including adoption support) in order to help our team members grow their families.  


## Parental Leave
GitLab's paid Parental Leave is for any active team member (regardless of gender) who becomes a parent through child birth or adoption while working at GitLab. 

**If you've been at GitLab for six months** and completed a [probationary period](/handbook/contracts/#probation-period) (if applicable), you can take up to **16 weeks of paid Parental Leave** during the first year of parenthood. This includes anyone who becomes a parent through childbirth or adoption while working for GitLab. When allowed by law the 16-week balance is per birth or adoption event. If you live in a country where a statutory Parental Leave benefit is available, you will be required to claim statutory Parental Leave pay (if you are eligible) and GitLab will supplement any gaps. 

We encourage parents to take the time they need. GitLab team members will be encouraged to decide for themselves the appropriate amount of time to take and how to take it. For many reasons, a team member may require more time off. Many GitLab team members are from countries that have longer standard Parental Leaves or births can have complications and sometimes 16 weeks just isn't enough. Any GitLab team member can request additional unpaid leave for up to 4 weeks. We are happy to address anyone with additional leave requests on a one-on-one basis. The entire Parental Leave balance must be used prior to the child's first birthday or the first anniversary of the adoption.

#### If you haven't been at GitLab for six months

Initially, you can take up to 25 consecutive calendar days of paid Parental Leave from the time of birth or adoption event which will count against the 16-week total. Full salary supplementation from GitLab will begin once you hit six months of service for the remaining Parental Leave balance. Depending on where you live, you may be able to receive statutory Parental Leave pay from the government to cover any unpaid time off.

### How to Initiate Your Parental Leave

Some teams require more time to put a plan of action in place so we recommend communicating your plan to your manager at least 2-3 months before your leave starts. In the meantime, familiarize yourself with the steps below and specific leave requirements in your [country](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#general-and-entity-benefits) (if any). 

**To initiate your Parental Leave, submit your time off by selecting the Parental Leave category in PTO by Roots at least 30 days before your leave starts.** We understand that Parental Leave dates may change. You can edit your PTO by Roots at a later time if you need to adjust the dates of your Parental Leave. It's important that you submit a tentative date at least 30 days in advance. Your manager and the Absence Management team will get notified after you submit your leave. The Absence Management team will send you an e-mail within 48 hours (whenever possible) confirming that they've been notified of your Parental Leave dates.

Please note, even though we have a "no ask, must tell" PTO, some countries require extra paperwork or notifications to a PEO so it's important that the Absence Management team is aware of your leave **at least 30 days before your leave starts.**

When you return from Parental Leave, please [notify the Absence Management that you have returned](/handbook/total-rewards/benefits/general-and-entity-benefits/#confirming-your-return).

### Planning Your Parental Leave Dates

Your 16 weeks of Parental Leave starts when the child arrives, but in some cases a team member may choose to begin their leave earlier. While we encourage you to begin your leave when your child arrives to be able to capture as much time as possible with the newest addition to your family, if you would like to start your leave on a different date please notify leaves@gitlab.com; otherwise the start date will be assumed as the date your child arrives.  

You don't have to take your Parental Leave in one continuous period, we encourage you to plan and arrange your Parental Leave in a way that suits you and your family's needs. You may split your Parental Leave dates as you see fit, so long as it is completed prior to the child's first birth or first anniversary of the adoption event. The Absence Management team will receive a notification every time you edit your Parental Leave dates. Make sure your leave is under the `Parental Leave` category, otherwise the Absence Management team won't get a notification.

**A few things to note:**
* If you are planning to change or extend your Parental Leave by using a different type of leave such as PTO, unpaid leave or any local statutory leave, please send an e-mail to leaves@gitlab.com.
*When determing the dates of your leave, GitLab uses a 1/7 formula (1 week = 7 days).  This means if you are planning on returning to work on a Monday, the last day of your leave will be Sunday.  Please use this formula when submitting your PTO by Roots request.

### Taking PTO After Taking Parental Leave

If you need to take more time off after taking 16 weeks of Parental Leave, you can utilize our [Paid Time Off policy](https://about.gitlab.com/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off). You'll need to return for at least 7 calendar days before taking any further time off since the full 16 weeks of parental leave will have been used. 

Please e-mail leaves@gitlab.com if you'd like to make this request and submit your PTO dates in [PTO by Roots](https://about.gitlab.com/handbook/paid-time-off/#pto-by-roots).

### Commission Payout and Quota Relief

After six months of tenure, commissions (if applicable) are paid while on parental leave based on the prior twelve months of performance with a cap at 100% of plan. For example, if in the twelve months prior to starting parental leave you attained 85% of plan, you will be compensated at the same rate while on leave. On the day you return from leave and going forward, your commissions will be based on current performance only. The rate of commissions paid during your leave will be confirmed by the sales commissions team and communicated to the team member through the Absence Management team prior to your leave start date. If your country of employment has specific laws regarding the payout of commissions when on leave, those would supersede our policy.

Additionally, effective Q1 FY22, commissioned roles are eligible to receive quota relief during their parental leave if the team member has been with GitLab for eight months or longer. The team member will recieve credit and commissions as specified in their existing Participant Schedule that occur ***prior*** to the start of the leave.  After 60 continuous days of parental leave, quota relief will be applied to the team member on leave for the total duration of the leave up to a total of 4 months. The team member would recieve an amended Participant Schedule upon return from leave. That amended participant schedule will include a prorated On Target Incentive based off the number of days that a team member is in seat for the fiscal year. For example, If coming back from leave at the beginning of a fiscal year, the On Target Incentive will not be prorated. Please see a [reference slide here](https://docs.google.com/presentation/d/1pWM8UhJGL7G8w_QzLKM1q2Rqql4OKm7vLD9HSCKfT9g/edit#slide=id.p) for an example of how the quota relief process and calculation works. For more information on quota relief and commissions on leave, please refer to the terms in the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/#sales-compensation-plan).

Sales management has discretion on how to cover a team member's territory while they are on leave. However, if another team member is assigned to cover business for someone on leave, they should be assigned the opportunity in SFDC but not the account. Team members covering opportunities will be compensated on the opportunities that they close in their name. Reassigning accounts may trigger quota changes.

For Solution Architects and Management and Technical Account Managers and Management, payment will be calculated for individuals using actual team performance while on leave. These roles will not be eligible for quota relief during parental leave. 

For Sales roles with a utilization attainment, like the [Professional Services Engineer](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/#team-metrics), since the utilization attainment is based on delivering above 70% of available billable hours, we apply the baseline attainment linked to utilization attainment, 50%. 

At the end of 90 continuous days of parental or medical leave, a one time payment of the 50% billable utilization attainment will be paid.  Utilization payout during parental or medical leave is not based on prior individual performance, since projects identified for billable utilization are based on sales engagements and services attached, and not determined by individual performance.

On the day you return from leave, the utilization payout returns to the established utilization attainment ranges.

You are entitled to and need to comply with your local regulations. They override our policy.

Some countries require extra paperwork or have specific leave requirements, which are subject to change as legal requirements change. Please take a look at your [country's leave policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#entity-benefits).


### Returning from Parental Leave

#### Confirming your return

A day before you return from Parental Leave, you'll receive a PTO by Roots [Welcome Back message](https://about.gitlab.com/handbook/people-group/engineering/slack-integrations/#parental-leave-welcome-back) with buttons to confirm your return or to explore other options.   
* Clicking the `Confirm my return` button will open the email client with a pre-loaded template "Confirming my return from Parental PTO".
* Clicking the `Explore options` button will also open the email client with a pre-loaded template "I have exceeded my 16 weeks and like to look into further options".

#### Return-to-work Resources

To [alleviate the stress](/handbook/paid-time-off/#returning-to-work-after-parental-leave) associated with returning to work after parental leave, GitLab supports team members coming back at [50% capacity](/handbook/paid-time-off/#returning-to-work-at-50-capacity). Parents at GitLab who are reentering work following parental leave are encouraged to read the [Team Member Tool Kit](/handbook/total-rewards/benefits/parental-leave-toolkit/#team-member-tool-kit) and to reach out to team members who self-designate as a [Parental Leave Reentry Buddy](/handbook/total-rewards/benefits/parental-leave-toolkit/#parental-leave-reentry-buddies).

Managers of soon to be parents should check out this [Parental Leave Manager Tool Kit](/handbook/total-rewards/benefits/parental-leave-toolkit/#manager-tool-kit) for best practices in supporting your team members as they prepare for and return from Parental Leave.

If you're interested in learning about how other GitLab team members approach parenthood, take a look at [the parenting resources wiki page](https://gitlab.com/gitlab-com/gitlab-team-member-resources/wikis/parenting) and [#intheparenthood](https://gitlab.slack.com/messages/CHADS8G12/) on Slack.


### Administration of Parental Leave Requests

For the Absence Management team:

  * PTO by Roots will notify the Absence Management Team of any upcoming parental leave.
  * Log and monitor upcoming parental leave in the "Parental Leave Log" Google sheet on the drive.
  * Notify the team member that the parental leave request was received by sending the confirmation e-mail template (only send this e-mail if the team member is starting their parental leave. Team members who are taking their leave intermittently or changing the dates of their original leave request do not need to receive the confirmation e-mail again).
  * Notify payroll - add the start date and end date on the payroll changes file ("Payroll Changes" sheet for team members in the US, "Canada Payroll Changes" sheet for team members in Canada and "Non-US Payroll Changes" sheet for everyone else). Ensure the entry is under the appropriate pay cycle date.
  * PTO by Roots will automatically update the employment status in BambooHR. Note: If the team member changes the date of their parental leave or cancels their request, the BambooHR employment status will need to be updated manually. 
  * Check local statutory requirements and if applicable, arrange any paperwork and liaise with appropriate parties (such as the PEO and payroll team) to initiate the parental leave.
  * 3 days after the End of Parental Leave date, BambooHR will send a notification to the Absence Management team. Check if the team member has confirmed their return via email. If not, send the Return to Work e-mail to the team member and manager.
  * Do a weekly audit of parental leave return dates. If a team member hasn't sent an e-mail within 3 days of returning, send them the return to work e-mail, copying the manager in.
  * If there are no changes to the End of Parental Leave date, update the team member's BambooHR status to "Active" and mark the status as "Completed" in the Parental Leave Log sheet.
  * If the dates in PTO by Roots change, update the payroll changes file and the BambooHR status accordingly.
