pages:
  - path: business-decision-makers
    content:
      title: Azure DevOps vs. GitLab for Business Decision Makers
      description: Learn more about the strenghts, limitations and differentiators between Azure DevOps and GitLab.
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: For Business Decision Makers
      css: extra-content-devops-tools.css
      page_body: |
        ## On this page
        {:.no_toc}

        - TOC
        {:toc}

        ## Azure DevOps Overview
        On September 10, 2018 Microsoft renamed VSTS to Azure DevOps and in Q1 2019 renamed TFS to Azure DevOps Server, and upgraded both with the same new user interface.

        Azure DevOps (VSTS) is a hosted cloud offering, and Azure DevOps Server (TFS), is an on-premises version. Both offer services that cover multiple stages of the DevOps lifecycle including planning tools, source code management (SCM), and CI/CD. Below is a infographic that aligns Azure DevOps services to the stages of the DevOps Lifecycle. Keep in mind that in many cases, when customers say they are using Azure DevOps, they are usually not consumers of the entire Azure DevOps breath of services. Azure DevOps users typically fall onto one of four categories:

          * Using Source Control Management from Microsoft
          * Using Requirements Management from Microsoft
          * Using CI/CD from Microsoft
          * Using TFS / VSTS from Microsoft

          ![Azure Toolchain](/devops-tools/azure_devops/images/Azure-Toolchain.png "Azure Toolchain")

        ## Azure DevOps Strengths

           * Single pre-integrated solutions for DevOps are starting to resonate.  Multiple data points indicate that the market is starting to perceive Microsoft And GitLab as the top two end to end DevOps solutions today.  Azure DevOps is viewed as a unified suite with a single user interface

           * Microsoft has strong enterprise relationships and multi-year ELAs.  ELAs make it easy for customers to pick and choose the products they need with minimal friction from the “buffet” of Microsoft choices under the ELA. Microsoft products only have to be “good enough” for the convenience that ELAs offer. Azure DevOps users are happy with Azure DevOps and see it as a single app for most of their flow/work - integrated “enough” for what they need to do now.

           * GitLab lacks Manual Testing Management capabilities, while Azure DevOps provides Azure Test Plans built-in for a separate licensing fee.

           * Azure DevOps MarketPlace and GitHub MarketPlace are key differentiators for Microsoft in terms of providing extensibility to their platforms.  Customers want the  ability to extend Azure DevOps/GitHub and to integrate with their existing tools.

           * Microsoft has a strong partner ecosystem in place today to enable a broader reach and adoption of its offerings.

           * Microsoft’s breadth remains both its key strength and weakness.  Microsoft’s strength is the wide range of Office productivity tools, developer tools, and enterprise IT management solutions that are pervasive in many enterprises.

        ## Azure DevOps Limitations and Challenges

          * Microsoft dev tool customers are not universally positive about Azure DevOps and/or GitHub.  Those customers who _have chosen to move to Azure DevOps and/or GitHub _generally like the Azure DevOps / GitHub tools.  However, the larger Microsoft tool customer base (legacy TFS/VSTS and others) are not too eager to be pushed into Azure DevOps and/or GitHub.

          * Microsoft does a good job of being extensible through their Marketplace for plugins although the Marketplace offerings are similar to Jenkins plugins (not all MS owned or guaranteed, many not rated and of questionable reliability). Under the covers, the complexity of multi-app shows when using Azure DevOps with plugins.  Plugins can be costly to maintain and support.

          * Microsoft’s breadth is also a weakness driven by inherent corporate and organizational complexity (multiple competing business units within the same structure).  This complexity creates multiple product and pricing/licensing options, product group competition and misalignment in business goals.

          * Microsoft customers still do not believe they are a cloud “neutral vendor”, believing that working with Microsoft predisposes them to being locked in with a single vendor. This is despite Microsoft itself appearing to have genuinely embraced a vendor-neutral, run anywhere philosophy (example - ability to work with K8, etc.).  For example, their messaging states - “Continuously build, test, and deploy to any platform and cloud.”

        ## Azure DevOps Selling Strategy

          * Microsoft’s target TAM for developer tools investments extends beyond the immediate DevOps tools TAM.  Microsoft’s DevOps solution offering is a means to grow their market share in the Cloud TAM which is approximately $69b. This means Microsoft will be willing to sacrifice DevOps tools in order to win Cloud business.

          * Microsoft is working to build stickiness around long term dependencies with Azure services**, in particular higher-value services like translation, image recognition, etc.

          * Microsoft has a strong partner ecosystem** for reaching a broader market and appropriate buyers (management/IT consulting and technology partners) and offering a complete solution for successful implementation (professional services).

        ## GitLab Differentiators

          * **Auto DevOps**: Want an automatic devops pipeline that takes your code into production without having to configure or maintain anything?  Azure DevOps has nothing like GitLab Auto DevOps. They have pipeline templates, but nothing that runs out of the box with such complete functionality.

          * **Canary and Incremental Roll Outs**: Azure DevOps does not have any out of the box release methodologies built in like GitLab does (granted, with GitLab, you need to be using Kubernetes to get these capabilities)

          * **No vendor lock-in**: Microsoft claims cloud-neutral, but will always make it easiest with Azure first.  They expects the majority of their revenue to come from selling cloud compute. They might claim cloud-neutral, but once you are on their platform they will do everything they can to get you to stay there. They’ve shown this behavior time and time again.  GitLab is independent of any cloud provider and is truly cloud-neutral.

          * **Application Security scanning**  Microsoft has no built-in tools to do SAST, DAST, Container scanning, dependency scanning, or Open Source license compliance scanning, while GitLab offers extensive built-in application security scanning.

          * **Open Core**:  GitLab is [open core](https://about.gitlab.com/2016/07/20/gitlab-is-open-core-github-is-closed-source/) and anyone can contribute changes directly to the codebase, which once merged would be automatically tested and maintained with every change.

        ## GitLab vs Azure DevOps Innovation Pace

          * Microsoft has accelerated its rate of innovation.  Azure DevOps and GitHub are now releasing quicker than GitLab.
            * Evidence can be seen by an analysis of the [2019 Azure DevOps Services release notes](https://docs.microsoft.com/en-us/azure/devops/release-notes/2019/sprint-156-update) (showing 12 releases in 7 months - 2019-01-14 to 2019-08-12) and release cadence acceleration from a constant 4 to 3 weeks. The [GitHub changelog](https://github.blog/changelog/) also shows us that the GitHub team also has a high release cadence, releasing features on a 1-2 day cadence.

            * Their project manager shared that they are releasing on a 3-4 week pace. This seems evident based on their [published roadmap](https://docs.microsoft.com/en-us/azure/devops/release-notes/). The same project manager also shared that Azure DevOps Server (TFS) will be 3-4 months behind on adopting new features (also evident by their published roadmap). They are both from the same code base.
  - path: continuous-integration
    content:
      title: Azure DevOps vs GitLab for Continuous Integration
      description: Continuous Integration comparison between Azure DevOps and GitLab.
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: Continuous Integration
      css: extra-content-devops-tools.css
      page_body: |

        **Azure DevOps** provides an easy to use Marketplace of plugins/extensions which makes Azure DevOps’ extensibility prevalent at key places in the user experience, such as when authoring a pipeline.

        **GitLab** does not provide a marketplace of plugins for integrations into pipelines.

        GitLab also offers Progressive Delivery capabilities such as Feature Flags, Review Apps and Deployment Scenarios (canar, incremental, etc.), which Azure DevOps does not offer.

        How do we Compare:

        |                                                                     | GitLab | Azure DevOps |
        |---------------------------------------------------------------------|--------|--------------|
        | Includes previous Release Manager (release pipelines)               |   Yes  |      Yes     |
        | Native container support                                            |   Yes  |      Yes     |
        | Save to any container registry                                      |   Yes  |      Yes     |
        | Linux, Windows cloud hosted agents                                  |   Yes  |      Yes     |
        |  macOS cloud hosted agents                                          |  Soon  |      Yes     |
        | Deployment stages                                                   |   Yes  |      Yes     |
        | Release gates, and approvals                                        |   No   |      Yes     |
        | Maven, npm, and NuGet package feeds from public and private sources |   No   |      Yes     |
        | Caching proxy of external repos/feeds                               |   No   |      Yes     |
        | Artifacts integrate natively with pipelines                         |   Yes  |      Yes     |
  - path: continuous-delivery
    content:
      title: Azure DevOps vs GitLab for Continuous Delivery
      description: Continuous Delivery comparison, highlights capabilities not native to Azure DevOps but available natively in GitLab.
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: Continuous Integration
      css: extra-content-devops-tools.css
      page_body: |

        **Azure DevOps**

        For support of traditional Release Management, Azure DevOps provides many primitives which GitLab doesn’t yet have but is working on, such as Release Approval Gates and Release Auditing.

        **GitLab**

        GitLab offers Progressive Delivery capabilities such as Feature Flags, Review Apps and Deployment Scenarios (canar, incremental, etc.), which Azure DevOps does not offer.
  - path: devsecops
    content:
      title: Azure DevOps vs GitLab for DevSecOps
      description: Azure DevOps DevSecOps capabilities are only through integrations. Highlights differences between Azure DevOps and GitLab.
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: DevSecOps
      css: extra-content-devops-tools.css
      page_body: |

        **Azure DevOps** has Application security available through integration with partner products.  They have **no** built-in tools to do SAST, DAST, Container scanning, dependency scanning, or Open Source license compliance scanning.

        In Azure DevOps, the results from non-built-in security scanning tools are not all available from the Merge/Pull Request or pipeline run, and the results are not formatted and presented consistently across the tools.

        You can integrate other tools to Azure Pipelines, but you have to install and maintain each one separately. And every integrated tool (plugin) has a different required configuration to be learned and done.  If not using OSS tools, then there is an additional licensing cost for each new tool.

        **GitLab** offers extensive built-in application security scanning.
  - path: version-control
    content:
      title: Azure DevOps vs GitLab for Version Control and Collaboration
      description: Azure DevOps' two options for Version Control and side-by-side comparison with GitLab.
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: Version Control and Collaboration
      css: extra-content-devops-tools.css
      page_body: |
        ## Version Control Overview


        **Azure DevOps** offers two methods of version control as part of their SCM functionality for both the SaaS and on-premie versions:

        * [Git](https://www.visualstudio.com/team-services/git/) (distributed) - each developer has a copy on their dev machine of the source repository including all branch and history information.

        * [Team Foundation Version Control (TFVC)](https://www.visualstudio.com/team-services/tfvc/)), a centralized, client-server system - developers have only one version of each file on their dev machines. Historical data is maintained only on the server.

        Microsoft recommends customers use Git for version control unless there is a specific need for centralized version control features: [See here](https://docs.microsoft.com/en-us/vsts/tfvc/comparison-git-tfvc).  This is noteworthy given that in June of 2018 Microsoft purchased [GitHub](/devops-tools/github-vs-gitlab/), the Internets largest online code repository.

        **GitLab** does not provide support for Team Foundation Version Control, however, this is not a highly desired version control method.

        How do we compare:

        |                                    | GitLab | Azure DevOps |
        |------------------------------------|--------|--------------|
        | Unlimited private Git repo hosting |   Yes  |      Yes     |
        | Diff in-line threaded code reviews |   Yes  |      Yes     |
        |   Branch policies defining merge   |   Yes  |      Yes     |
        |            Pull requests           |   Yes  |      Yes     |
        |        Semantic code search        |   Yes  |      Yes     |
        |       Webhooks and REST APIs       |   Yes  |      Yes     |

        ## Collaboration Overview

        **Azure DevOps** provides persona-centric dashboards and views that resonate with customers, allowing them to reach not only the developers but the mindshare of executive leadership and therefore, sell higher in the enterprise.

        **GitLab** has some built-in graphs and dashboards, but nothing as extensive as the Azure DevOps offering, and not targeted at higher roles in the enterprise.

        How do we compare:

        |                                                                        | GitLab | Azure DevOps |
        |------------------------------------------------------------------------|--------|--------------|
        |                           Work/issue tracking                          |   Yes  |      Yes     |
        |                                Backlogs                                |   Yes  |      Yes     |
        |                             Team dashboards                            |   No   |      Yes     |
        |                            Custom reporting                            |   No   |      Yes     |
        |                              Kanban boards                             |   Yes  |      Yes     |
        |                    Scrum boards and sprint planning                    |   Yes  |      Yes     |
        |                    Customizable work item workflows                    |   Yes  |      Yes     |
        | Test & Feedback (exploratory/manual testing) - capture & record issues |   No   |      Yes     |
        |                   Test planning, tracking & execution                  |   No   |      Yes     |
        |                Load testing (Azure DevOps and VSTS only)               |   No   |      Yes     |
        |                         User acceptance testing                        |   Yes  |      Yes     |
        |                          Centralized reporting                         |   Yes  |      Yes     |
  - path: licensing
    content:
      title: Azure DevOps Licensing
      description: Azure DevOps' multiple licensing drivers for different product components.
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: Licensing
      css: extra-content-devops-tools.css
      page_body: |
        ## On this page
        {:.no_toc}

        - TOC
        {:toc}

        ## Pipeline Costs
        * Open source (public) projects get 10 free parallel jobs, unlimited time
        * private projects - MS hosted - 1 free parallel job, 1800 mins/mnth - $40/parallel addition, unlimited time
        * private projects - self-hosted - 1 free parallel job, unlimited time, each Visual Studio Enterprise subscriber in org = 1 additional self-hosted parallel job, beyond that +$15 each additional parallel job
            * ***NOTE*** Their pricing is pushing folks to their cloud service offering. On-prem Pipelines Server = buying TFS licenses (now called Azure DevOps Server). This version will be updated 3-4 months behind in feature/functionality updates.
            * [Azure Pipelines Only Pricing](https://azure.microsoft.com/en-us/pricing/details/devops/azure-pipelines/)

        ## Artifacts Costs
        * Free <=5 users
        * additional $4/user above 5

        ## Repos Costs
        * Free <=5 users
        * Included in $30/month per 10 users

        ## Boards Costs
        * Free <=5 users
        * Included in $30/month per 10 users

        ## Test Plan Costs
        * additional $52/month per user

        ## Azure DevOps Add-Ons

        **Separate from Azure DevOps but available for extra cost**

        * Azure Monitor - APM, infra, data, services **(GitLab has)**
            * App centric
            * but separate from Azure DevOps.
            * [https://docs.microsoft.com/en-us/azure/azure-monitor/overview](https://docs.microsoft.com/en-us/azure/azure-monitor/overview)
            * pay by use
        * Visual Studio - Full blown IDE - Free with sub
        * Visual Studio Code - IDE Lite - Free  **(GitLab has)**
        * Container Registry (MS has Azure Container Registry) **(GitLab has)**

        ## Microsoft Pricing Links

        **Azure DevOps**

        * [Azure DevOps Services Pricing](https://azure.microsoft.com/en-us/pricing/details/devops/azure-devops-services/)
        * [Azure DevOps On-prem](https://azure.microsoft.com/en-us/pricing/details/devops/on-premises/) = See TFS Pricing

        **Visual Studio**

        * Current VSTS and MSDN subscribers get different levels of Azure DevOps. Details can be found at [Azure DevOps for Visual Studio subscribers](https://docs.microsoft.com/en-us/visualstudio/subscriptions/vs-azure-devops)

        * [VSTS Pricing](https://visualstudio.microsoft.com/team-services/pricing/)

        Visual Studio ‘Professional Version’ is the most comparable to GitLab since Visual Studio ‘Enterprise Version’ includes extras outside the scope of DevOps (such as MS Office, etc).

        Visual Studio Professional can be purchased under a ‘standard’ or ‘cloud’ model.

        - Standard = $1,200 year one (retail pricing), then $800 annual renewals (retail pricing)
        - Cloud - $540 per year

        Under their ‘modern purchasing model’, the monthly cost for Visual Studio Professional (which includes TFS and CAL license) is $45 / mo ($540 / yr).  However, extensions to TFS such as [Test Manager](https://marketplace.visualstudio.com/items?itemName=ms.vss-testmanager-web) ($52/mo), [Package Management](https://marketplace.visualstudio.com/items?itemName=ms.feed) ($15/mo), and [Private Pipelines](https://marketplace.visualstudio.com/items?itemName=ms.build-release-private-pipelines) ($15/mo) require an additional purchase.

        **Team Foundation Server**

        [TFS Pricing](https://visualstudio.microsoft.com/team-services/tfs-pricing/)

        A TFS license can be purchased as standalone product, but a TFS license (and CAL license) is also included when you buy a Visual Studio license / subscription.

        MS pushes Visual Studio subscriptions and refers customers who are only interested in a standalone TFS with a ‘classic purchasing’ model to license from a reseller.

        Excluding CapEx and Windows operating system license, a standalone TFS license through a reseller in classic purchasing model is approximately $225 per year per instance.  The approximate Client Access License is approximately $320 per year.

        ## General Notes
        * All paid plans include unlimited stakeholder users who can view and contribute to work items and boards, and view dashboards, charts, and pipelines
        * Release details and [roadmap](https://docs.microsoft.com/en-us/azure/devops/release-notes/)
        * All current VSTS subscribers will be moved automatically to Azure DevOps.
        * TFS (on prem) pricing implies a SaaS first mentality and customer push
            * Buy at least one Visual Studio license + Azure DevOps users @ $6/mnth
                * Visual Studio Professional ($45/mnth) - no Test Manager, Artifacts, Pipelines (unless OSS)
                * Visual Studio Enterprise ($250/mnth) - include Test Manager and Artifacts
            * [https://visualstudio.microsoft.com/team-services/tfs-pricing/](https://visualstudio.microsoft.com/team-services/tfs-pricing/)

  - path: multi-stage-pipelines
    content:
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: GitLab vs. Azure DevOps Multi-Stage YAML Pipelines
      css: extra-content-devops-tools.css
      page_body: |

        In April 2020, Azure DevOps introduced Multi-Stage YAML pipelines for CI/CD.   The Multi-Stage pipelines feature allows users the ability to execute the CI, CD or both functions within their YAML pipeline (file), a practice that GitLab has already been exercising.

        The primary difference in GitLab’s approach to Multi-Stage Pipelines and Azure DevOps approach is the structure and correlation of “Stages” to “Jobs” within the YAML pipeline file.

        ## GitLab Multi-stage Pipelines

        [GitLab Stages](https://docs.gitlab.com/ee/ci/yaml/) are defined globally for the pipeline.  Each Job executed within the YAML pipeline identifies a Stage that it correlates to.  Here is an example of how GitLab Stages and Jobs are structured in the YAML pipeline:

        ![GitLab Multi State Pipelines](/images/devops-tools/gitlab-pipelines.png)


        ## Azure DevOps Multi-stage Pipelines

        [Azure DevOps Stages](https://docs.microsoft.com/en-us/azure/devops/pipelines/yaml-schema?view=azure-devops&tabs=schema%2Cparameter-schema) are defined as divisions within the pipeline.  Each Stage executed within the YAML pipeline identifies Jobs to run.  Here is an example of how Azure DevOps Stages and Jobs are structured in the YAML pipeline:

        ![AzureDevOps Multi State Pipelines](/images/devops-tools/azure-pipelines.png)

        ## Conclusion

        GitLab’s approach to configuring and structuring the Multi-stage YAML pipeline is easier to consume and less complicated than Azure DevOps approach.  With GitLab, Job definitions include a Stage label to indicate at which Stage that Job should be executed (as seen above).  With this approach, adding new attributes to a Job or even creating templates is much simpler due to how Stage definitions and Job definitions are separate within the structure of the YAML Pipeline.  Azure DevOps approach, however, is more complicated since they group Job definitions within the Stage definitions (as seen above).  Adding and deleting attributes to a Job within a Stage could present configuration confusion.
